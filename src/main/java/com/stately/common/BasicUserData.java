package com.stately.common;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import java.io.Serializable;

/**
 *
 * @author Edwin
 */
public class BasicUserData implements Serializable
{
    
    private String userAccessCode;

    private String fullName;
    private boolean hasUserLogin = false;
    
    private String userRole;

    private String userId;
    

    private boolean userHasAdministrativeRight;
    
    public boolean isUserInRole(String fullRoleCode)
    {
        return userAccessCode.contains(fullRoleCode);
    }

    public String getRoleCode(String pageCode, String roleCode)
    {
        return pageCode + "#" + roleCode;
    }

    public  AccessRightChecker on(String pageCode)
    {
        AccessRightChecker checker = new AccessRightChecker(pageCode);

        return checker;
    }


    public class AccessRightChecker
    {

        private String pageCode;

        public AccessRightChecker(String pageCode)
        {
            this.pageCode = pageCode;
        }

        public boolean has(String role)
        {
            String fullCode = getRoleCode(pageCode, pageCode);

            return userAccessCode.contains(fullCode);
        }
    }



    // <editor-fold defaultstate="collapsed" desc="Getter and Setter">

    void dd()
    {

    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getUserAccessCode()
    {
        return userAccessCode;
    }

    public void setUserAccessCode(String userAccessCode)
    {
        this.userAccessCode = userAccessCode;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserRole()
    {
        return userRole;
    }

    public void setUserRole(String userRole)
    {
        this.userRole = userRole;
    }

    public boolean isHasUserLogin()
    {
        return hasUserLogin;
    }

    public void setHasUserLogin(boolean hasUserLogin)
    {
        this.hasUserLogin = hasUserLogin;
    }

    public boolean isUserHasAdministrativeRight()
    {
        return userHasAdministrativeRight;
    }

    public void setUserHasAdministrativeRight(boolean userHasAdministrativeRight)
    {
        this.userHasAdministrativeRight = userHasAdministrativeRight;
    }


    
    // </editor-fold>
}
