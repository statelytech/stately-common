/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.common;

import java.io.Serializable;

/**
 *
 * @author Edwin
 */
public class FormAction implements Serializable
{
    private boolean create;
    private boolean list;

    public void clearAll()
    {
        create = false;
        list = false;
    }
    
    public boolean isCreate()
    {
        return create;
    }

    public void setCreate(boolean create)
    {
        this.create = create;
    }

    public boolean isList()
    {
        return list;
    }

    public void setList(boolean list)
    {
        this.list = list;
    }
    
    
}
