package com.stately.common;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import com.stately.common.utils.StringUtil;
import java.net.URL;
import java.io.IOException;
import java.awt.Image;
import java.awt.Graphics;
import java.awt.image.ImageObserver;
import javax.swing.JFrame;
import javax.swing.JComponent;

// Written by John Catherino
// Project Lead
// The cajo project: https://cajo.dev.java.net
// usage: http://code.google.com/apis/maps/documentation/staticmaps/
public final class GoogleMap extends JComponent implements ImageObserver
{

    static final String http = "http://maps.google.com/maps/api/staticmap?";
    Image map;

    public GoogleMap(String params) throws IOException
    {
        fetch(params);
    }

    public void fetch(String params) throws IOException
    { // get another map
        if (map != null)
        {
            map.flush();
        }
        java.net.URLConnection con = new URL(http + params).openConnection();
        java.io.InputStream is = con.getInputStream();
        byte bytes[] = new byte[con.getContentLength()];
        for (int x = is.read(bytes, 0, bytes.length);
                x < bytes.length;
                x += is.read(bytes, x, bytes.length - x));
        is.close();
        java.awt.Toolkit tk = getToolkit();
        map = tk.createImage(bytes);
        tk.prepareImage(map, -1, -1, this);

        for (int i = 0; i < bytes.length; i++)
        {
            byte b = bytes[i];
            System.out.println(b);

        }
    }

    public boolean imageUpdate(Image i, int f, int x, int y, int w, int h)
    {
        if ((f & WIDTH + HEIGHT) == WIDTH + HEIGHT)
        {
            setSize(w, h);
        }
        if (isShowing() && (f & (ALLBITS | FRAMEBITS)) != 0)
        {
            repaint();
        }
        return isShowing();
    }

    public java.awt.Dimension getPreferredSize()
    {
        return getSize();
    }

    public void paint(Graphics g)
    {
        g.drawImage(map, 0, 0, this);
    }

    public static void main(String... args) throws IOException
    { // unit test

//        Brooklyn+Bridge,New+York,NY
        
        JFrame frame = new JFrame("Google Map");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(new GoogleMap(
                "center=buokrom&"
                + "zoom=14&size=512x512&maptype=roadmap&sensor=false&"
                + "markers=color:blue|label:S|40.702147,-74.015794&"
                + "markers=color:green|label:G|40.711614,-74.012318&"
                + "markers=color:red|label:C|40.718217,-73.998284"));
        frame.pack(); // small race condition, image may not be fully prepared
        frame.setVisible(true);
    }
}
