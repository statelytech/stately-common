/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.common;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Edwin
 */
public class PartitionerHelper<T>
{

    private List<T> inputList;

    private ExecutorService executorService = null;
    private int CHUNCK_SIZE = 500;
    private List<List<T>> partitionsList;

    public PartitionerHelper(int chuckSize, List<T> inputList)
    {
        this.CHUNCK_SIZE = chuckSize;
        this.inputList = inputList;

        int numberOfThreds = inputList.size() / CHUNCK_SIZE;

        if (numberOfThreds == 0)
        {
            numberOfThreds = 1;
        }

        executorService = Executors.newFixedThreadPool(numberOfThreds);

        partitionsList = Lists.partition(inputList, CHUNCK_SIZE);
    }

    public ExecutorService getExecutorService()
    {
        return executorService;
    }

    public List<List<T>> getPartitionsList()
    {
        return partitionsList;
    }

}
