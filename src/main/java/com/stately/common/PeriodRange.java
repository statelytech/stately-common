/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.common;

import com.stately.common.utils.MonthPeriod;
import java.io.Serializable;

/**
 *
 * @author Edwin
 */
public class PeriodRange implements Serializable
{
    private MonthPeriod startPeriod = new MonthPeriod();
    private MonthPeriod endPeriod = new MonthPeriod();

    public MonthPeriod getStartPeriod()
    {
        return startPeriod;
    }

    public void setStartPeriod(MonthPeriod startPeriod)
    {
        this.startPeriod = startPeriod;
    }

    public MonthPeriod getEndPeriod()
    {
        return endPeriod;
    }

    public void setEndPeriod(MonthPeriod endPeriod)
    {
        this.endPeriod = endPeriod;
    }
    
}
