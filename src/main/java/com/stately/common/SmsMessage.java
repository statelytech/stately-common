/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.common;

import com.google.gson.Gson;
import com.stately.common.utils.HttpConResponse;
import com.stately.common.utils.HttpConnectionUtils;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Edwin
 */
public class SmsMessage
{

    public static final String BULK_SMS_URL = "http://clientlogin.bulksmsgh.com/api/smsapi";
    public static final String SMS_GH = "https://api.smsgh.com/v3/messages/send";
    public static final String infoBibUrl = "https://292rp.api.infobip.com/sms/1/text/single";
//    public static final String infoBibUrl = "https://api.infobip.com/sms/1/text/single";
    
    
     private static final String _authorizationHeader = "Authorization";

    private SmsProvider smsProvider = SmsProvider.BULK_SMS_GHANA;

    public SmsMessage(SmsProvider smsProvider)
    {
        this.smsProvider = smsProvider;
    }

    private String msg;
    private String to;
    private String sender;
    private String apiToken;
    private String clientId;
    private String ClientSecret;
    
    private String failureMsg = "";
    
    
    
    public boolean sendMessage(String phoneNo, String message) 
    {
        if (isNullOrEmpty(message) || isNullOrEmpty(phoneNo)) 
        {
            System.out.println(message + " -- " + phoneNo);
            return false;
        }
        
        if(phoneNo.startsWith("0"))
        {
            phoneNo = phoneNo.replaceFirst("0", "233");
        }
        
        
        if(smsProvider == SmsProvider.SMSGH)
        {
            return sendMessageHubtel(phoneNo, message);
        }
        else if(smsProvider == SmsProvider.INFOBIP)
        {
            return sendMessageInfoBib(phoneNo, message);
        }
        
        try {

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    
    private boolean sendMessageHubtel(String phoneNo, String message) 
    {
        if (isNullOrEmpty(message) || isNullOrEmpty(phoneNo)) {
            System.out.println(message + " -- " + phoneNo);
            return false;
        }

        HttpURLConnection conn = null;
        try {

//            bulkSmsGH.setTo("0277115150");
            setTo(phoneNo.trim());
            setMsg(message);

            String fullUrl = getFullPath();
////                String fullUrl = baseUrl + "?" + param;

        
//
            System.out.println("fullUrl : " + fullUrl);
//
            URL url = new URL(fullUrl);

            conn = (HttpURLConnection) url.openConnection();

            if (isSucessfull(conn)) {
//                holderScheduledStatement.setSentStatus(SentStatus.SENT);
//                holderScheduledStatement.setSentDate(new Date());
//                holderScheduledStatement.setRemarks("SMS Sent");
//                crudService.save(holderScheduledStatement);

                return true;

            } else {
//                holderScheduledStatement.setRemarks("Not Sent");
//                holderScheduledStatement.setSentStatus(SentStatus.PENDING);
//                holderScheduledStatement.setSentDate(new Date());
//                crudService.save(holderScheduledStatement);

                return false;

            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }
    
    
    
    private boolean sendMessageInfoBib(String phoneNo, String message) 
    {
        if (isNullOrEmpty(message) || isNullOrEmpty(phoneNo)) {
            System.out.println(message + " -- " + phoneNo);
            return false;
        }

//        HttpURLConnection conn = null;
        try {

            String jsonDoc = "{"
                    +"\"from\":\""+sender+"\","
                    +"\"to\":\""+phoneNo+"\","
                    +"\"text\":\""+message+"\""
                    +"}";
            
            Map<String, String> map = new LinkedHashMap<>();
            map.put(_authorizationHeader, "Basic " + basicAuth());
//            map.put(_authorizationHeader, "Basic " + basicAuth());
//             System.out.println(map);
//            HttpConnectionUtils.sendJsonPost(infoBibUrl, jsonDoc, map);
            
            
            String url = "https://292rp.api.infobip.com/sms/1/text/query?"
                    + "username="+clientId
                    +"&password="+ClientSecret
                    +"&to="+phoneNo+""
                    +"&sender="+sender+""
                    + "&text="+ URLEncoder.encode(message, "ISO-8859-1");
            
            HttpConResponse conResponse = HttpConnectionUtils.sendGet(url);
            
            System.out.println(conResponse);
            
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
    
    
    private String basicAuth() throws UnsupportedEncodingException
    {
        String authString = clientId + ":" + ClientSecret;
//           System.out.println("auth : " + authString);
        String endded = Base64.getEncoder().encodeToString(authString.getBytes("UTF-8"));
//           System.out.println("encoded : " + endded);
        return endded;
    }
    
    public boolean checkResult(String result)
    {
        if ("1000".equalsIgnoreCase(result))
        {
            return true;
        } else
        {
            return false;
        }
    }

    public boolean isSucessfull(HttpURLConnection httpURLConnection)
    {
        
        if(httpURLConnection == null)
        {
            return false;
        }
        
        
        
        
        try
        {
            InputStream inputStream = null;
            
            try
            {
                inputStream = httpURLConnection.getInputStream();
            } catch (Exception e)
            {
                inputStream = httpURLConnection.getErrorStream();
            }
//            if(httpURLConnection.getResponseCode() == 200)
//            {
//                
//            }
//            else
//            {
//                
//            }
            
            BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
            String result = IOUtils.toString(rd).trim();
            System.out.println("result : " + result);

            if (smsProvider == SmsProvider.BULK_SMS_GHANA)
            {
                return checkResult(result);
            } else if (smsProvider == SmsProvider.SMSGH)
            {
                SMSGhRespose mSGhRespose = new Gson().fromJson(result, SMSGhRespose.class);

                if (mSGhRespose == null)
                {
                    return false;
                }

                if ("0".equalsIgnoreCase(mSGhRespose.getStatus()))
//                if (mSGhRespose.getStatus().equalsIgnoreCase("0"))
                {
                    return true;
                }
                else
                {
                    failureMsg = mSGhRespose.getMessageId();
                    return false;
                }
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public String getFullPath()
    {
        return createSendString(sender);

    }

    public String createSendString(String smsSender)
    {
        String fullUrl = "";
        try
        {
            if (smsProvider == SmsProvider.BULK_SMS_GHANA)
            {
                String param = "key=" + apiToken + "&sender_id=" + smsSender + ""
                        + "&msg=" + URLEncoder.encode(msg, "UTF-8")
                        //                        + "&IsFlash=0&type=longSMS"
                        //                                                + "&to=233277115150";
                        + "&to=" + to.trim() + "";

                fullUrl = BULK_SMS_URL + "?" + param;
            } 
            else if (smsProvider == SmsProvider.SMSGH)
            {
                String param = "";
                param += "From=" + URLEncoder.encode(smsSender, "ISO-8859-1");
                param += "&ClientId=" + URLEncoder.encode(clientId, "ISO-8859-1");
                param += "&Content=" + URLEncoder.encode(msg, "ISO-8859-1");
                param += "&ClientSecret=" + URLEncoder.encode(ClientSecret, "ISO-8859-1") + "";
                param += "&RegisteredDelivery=true";
                param += "&To=" + to.trim();

                fullUrl = SMS_GH + "?" + param;
            }
            

//            System.out.println("fullUrl : " + fullUrl);

            return fullUrl;
        } catch (Exception e)
        {
        }

        return "";

    }
    
    static boolean isNullOrEmpty(String string) {
    return string == null || string.isEmpty();
  }

    public static void main(String[] args)
    {
        try
        {

            SmsMessage bulkSmsGH = new SmsMessage(SmsProvider.INFOBIP);

//            bulkSmsGH.setApiToken("0b9b204ca6bf4d3053b1");
            bulkSmsGH.setMsg("Sample Test Message");
            bulkSmsGH.setTo("0501338841");
            bulkSmsGH.setTo("0277115150");
//            bulkSmsGH.setTo("0244316190");


            bulkSmsGH.setSender("UPT-Pension");
            bulkSmsGH.setClientId("UPT1");
            bulkSmsGH.setClientSecret("Upt@12345");

//            String fullUrl = bulkSmsGH.getFullPath();
//
//            System.out.println("fullUrl : " + fullUrl);
//
//            URL url = new URL(fullUrl);
//
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//
//            System.out.println("Status : " + bulkSmsGH.isSucessfull(conn));
//            
//            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//            String result = IOUtils.toString(rd).trim();
//            System.out.println("result : " + result);



            String msg = "CONTRIBUTION NOTIFICATION \n"
                    + "Dear AMOAKWA EDWIN KWAME, \n"
                    + "Please we have received GHS500.00 being your PERSONAL PENSION\n"
                    + " Contribution (March 2019)\n"
                    + "Thank you.\n"
                    + "United Pension Trustees\n"
                    + "0302-208042\\0242-436880";

//            msg = "CONTRIBUTION NOTIFICATION "
//                    + "Dear AMOAKWA EDWIN KWAME, "
//                    + "Please we have received GHS500.00 being your PERSONAL PENSION"
//                    + " Contribution (March 2019)"
//                    + "Thank you."
//                    + "United Pension Trustees"
//                    + "0302-208042242-436880";

//            msg = "Hello Edwin at " + LocalDateTime.now();
                    
            bulkSmsGH.sendMessage("0277115150", msg);
//            bulkSmsGH.sendMessage("0277115150", "Hello Edwin " + LocalDateTime.now());
            

        } catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public String getTo()
    {
        return to;
    }

    public void setTo(String to)
    {
        this.to = to;
    }

    public String getSender()
    {
        return sender;
    }

    public void setSender(String sender)
    {
        this.sender = sender;
    }

    public String getApiToken()
    {
        return apiToken;
    }

    public void setApiToken(String apiToken)
    {
        this.apiToken = apiToken;
    }

    public SmsProvider getSmsProvider()
    {
        return smsProvider;
    }

    public void setSmsProvider(SmsProvider smsProvider)
    {
        this.smsProvider = smsProvider;
    }

    public String getClientId()
    {
        return clientId;
    }

    public void setClientId(String clientId)
    {
        this.clientId = clientId;
    }

    public String getClientSecret()
    {
        return ClientSecret;
    }

    public void setClientSecret(String ClientSecret)
    {
        this.ClientSecret = ClientSecret;
    }

    public String getFailureMsg()
    {
        return failureMsg;
    }
    

    public static class SMSGhRespose
    {

        private String Status;
        private String MessageId;
        private String Rate;
        private String NetworkId;
        private String ClientReference;

        public String getStatus()
        {
            return Status;
        }

        public void setStatus(String Status)
        {
            this.Status = Status;
        }

        public String getMessageId()
        {
            return MessageId;
        }

        public void setMessageId(String MessageId)
        {
            this.MessageId = MessageId;
        }

        public String getRate()
        {
            return Rate;
        }

        public void setRate(String Rate)
        {
            this.Rate = Rate;
        }

        public String getNetworkId()
        {
            return NetworkId;
        }

        public void setNetworkId(String NetworkId)
        {
            this.NetworkId = NetworkId;
        }

        public String getClientReference()
        {
            return ClientReference;
        }

        public void setClientReference(String ClientReference)
        {
            this.ClientReference = ClientReference;
        }
    }
    
    
}
