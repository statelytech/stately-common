/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.common;

/**
 *
 * @author ic securities
 */
public enum SmsProvider {
    BULK_SMS_GHANA("Bulk SMS Ghana"),   
    INFOBIP("INFOBIP"),   
    SMSGH("SMS GH");   
    
    String desc;

    private SmsProvider(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return  desc ;
    }
    
    
}
