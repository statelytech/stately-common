package com.stately.common;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
 
/**
* This utility compresses a list of files to standard ZIP format file.
* It is able to compresses all sub files and sub directories, recursively.
* @author Ha Minh Nam
*
*/
public class ZipUtil {
    /**
     * A constants for buffer size used to read/write data
     */
    
    private static final int BUFFER_SIZE = 4096;
    
    /**
     * Compresses a collection of files to a destination zip file
     * @param listFiles A collection of files and directories
     * @param destZipFile The path of the destination zip file
     * @throws FileNotFoundException
     * @throws IOException
     */
   public void compressFiles(List<File> listFiles, String destZipFile) throws FileNotFoundException, IOException {
        
       ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(destZipFile));
        
       for (File file : listFiles) {
           if(file == null)
           {
               continue;
           }
           if (file.isDirectory()) {
               addFolderToZip(file, file.getName(), zos);
           } else {
               addFileToZip(file, zos);
           }
       }
        
       zos.flush();
       zos.close();
   }
    
   /**
    * Adds a directory to the current zip output stream
    * @param folder the directory to be  added
    * @param parentFolder the path of parent directory
    * @param zos the current zip output stream
    * @throws FileNotFoundException
    * @throws IOException
    */
    private void addFolderToZip(File folder, String parentFolder,
            ZipOutputStream zos) throws FileNotFoundException, IOException {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) {
                addFolderToZip(file, parentFolder + "/" + file.getName(), zos);
                continue;
            }
 
            zos.putNextEntry(new ZipEntry(parentFolder + "/" + file.getName()));
 
            BufferedInputStream bis = new BufferedInputStream(
                    new FileInputStream(file));
 
            long bytesRead = 0;
            byte[] bytesIn = new byte[BUFFER_SIZE];
            int read = 0;
 
            while ((read = bis.read(bytesIn)) != -1) {
                zos.write(bytesIn, 0, read);
                bytesRead += read;
            }
 
            zos.closeEntry();
 
        }
    }
 
    /**
     * Adds a file to the current zip output stream
     * @param file the file to be added
     * @param zos the current zip output stream
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void addFileToZip(File file, ZipOutputStream zos)
            throws FileNotFoundException, IOException {
        zos.putNextEntry(new ZipEntry(file.getName()));
 
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(
                file));
 
        long bytesRead = 0;
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
 
        while ((read = bis.read(bytesIn)) != -1) {
            zos.write(bytesIn, 0, read);
            bytesRead += read;
        }
 
        zos.closeEntry();
    }
    
    public void decompressFile(String zipFilePath, String destDirectory) throws IOException {
        File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        
        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
        
        ZipEntry entry = zipIn.getNextEntry();
        
        while (entry != null) {
            String filePath = destDirectory + File.separator + entry.getName();
            if (!entry.isDirectory()) {
                extractFile(zipIn, filePath);
            } else {
                File dir = new File(filePath);
                dir.mkdir();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
    }
    
    /**
     * Extracts a single file
     * @param zipIn the ZipInputStream
     * @param filePath Path of the destination file
     * @throws IOException if any I/O error occurred
     */
    private void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }   
        bos.close();       
    }
 
}