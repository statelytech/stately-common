package com.stately.common.api;

/**
 *
 * @author Edwin
 */
public interface MessageResolvable
{
    String getCode();
    String getLabel();
}
