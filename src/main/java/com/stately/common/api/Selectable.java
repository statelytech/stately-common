/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.common.api;

/**
 *
 * @author Edwin
 */
public interface Selectable 
{

    boolean isSelected();

    void setSelected(boolean selected);
    
}
