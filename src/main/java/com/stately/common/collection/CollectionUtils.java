/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.stately.common.collection;

import com.stately.common.api.Selectable;
import com.stately.common.collection.comparators.StringValueComparator;
import com.stately.common.collection.comparators.TO_StringComparator;
import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author edwin
 */
public class CollectionUtils implements Serializable
{
    private final static Logger LOGGER = Logger.getLogger(CollectionUtils.class.getName());

    public static void sortStringValue(List list)
    {
        Collections.sort(list, StringValueComparator.instance());
    }
    
     public static void sortToString(List list)
    {
        Collections.sort(list, TO_StringComparator.instance());
    }
     
    public static void deSelectAll(List<? extends Selectable> selectables)
    {
        for (Selectable object : selectables)
        {
            try
            {
                object.setSelected(false);
            } catch (Exception e)
            {
                LOGGER.log(Level.SEVERE, "Error in deselecting passed selectables -" + object ,e);
            }
        }
    }

    public static void selectAll(List<? extends Selectable> selectables)
    {
        for (Selectable selectable : selectables)
        {           try
            {
                selectable.setSelected(true);
            } catch (Exception e)
            {
                LOGGER.log(Level.SEVERE, "Error in deselecting passed selectables " + selectable ,e);
            }
        }
    }
    
    public static void markSelected(List<? extends Selectable>  mailList, List<? extends Selectable>  sublist)
    {
        for (Selectable selectable : sublist)
        {
            for (Selectable sel :mailList )
            {
                if(selectable.equals(sel))
                {
                    sel.setSelected(true);
                    break;
                }
            }
        }
    }
    
    
    public static List checkAdd(List list, Object item)
    {
        if (list == null || list.isEmpty())
        {
            list = new LinkedList();
        }

        
        if (!list.contains(item))
        {
            list.add(0, item);
        }
        
        return list;
    }
}
