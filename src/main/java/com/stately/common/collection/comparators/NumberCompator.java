/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.stately.common.collection.comparators;

import java.util.Comparator;

/**
 *
 * @author edwin
 */
public class NumberCompator implements Comparator<Number>
    {
        @Override
        public int compare(Number o1, Number o2)
        {
            double firstValue = 0.0;
            double secondValue = 0.0;

            if(o1 != null)
            {
                firstValue = o1.doubleValue();
            }

            if(o2 != null)
            {
                secondValue = o2.doubleValue();
            }

            System.out.println("values comparing ... " + firstValue + " sec: " +secondValue);

            if (firstValue == secondValue)
            {
                return 0;
            } else if (firstValue < secondValue)
            {
                return -1;
            } else
            {
                return 1;
            }
        }
}
