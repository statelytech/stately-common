/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.common.collection.comparators;

/**
 *
 * @author Edwin
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StringValueComparator  implements Comparator<Object>
{


    private static StringValueComparator valueComparator = new StringValueComparator();

    public static StringValueComparator instance()
    {
        return valueComparator;
    }

    public static void sort(List listCollections)
    {
        if(listCollections == null)
            return;

        Collections.sort(listCollections, valueComparator);
    }


    public int compare(Object o1, Object o2)
    {
        String s1 = o1.toString().trim();
        String s2 = o2.toString().trim();

        if (s1.isEmpty())
        {
            return -1;
        }
        if (s2.isEmpty())
        {
            return 1;
        }
        if (s1.charAt(0) == s2.charAt(0))
        {
            s1 = s1.substring(1, s1.length());
            s2 = s2.substring(1, s2.length());

            if (s1.isEmpty())
            {
                return -1;
            }
            if (s2.isEmpty())
            {
                return 1;
            }
            if ((s1.length() < s2.length()) && (s2.charAt(0) != '.'))
            {
                s1 = "0" + s1;
            } else if ((s1.length() > s2.length()) && (s1.charAt(0) != '.'))
            {
                s2 = "0" + s2;
            }
            return compare(s1, s2);
        }

        int n1 = s1.length();
        int n2 = s2.length();

        int i1 = 0;
        for (int i2 = 0; (i1 < n1) && (i2 < n2); i2++)
        {
            char c1 = s1.charAt(i1);
            char c2 = s2.charAt(i2);
            if (c1 != c2)
            {
                c1 = Character.toUpperCase(c1);
                c2 = Character.toUpperCase(c2);
                if (c1 != c2)
                {
                    c1 = Character.toLowerCase(c1);
                    c2 = Character.toLowerCase(c2);

                    if (c1 != c2)
                    {
                        return c1 - c2;
                    }
                }
            }
            i1++;
        }

        System.out.println(": " + n1 + "   > " + n2);

        return s1.charAt(0) - s2.charAt(0);
    }

    private int value(String stringVal)
    {
        int val = 0;

        char[] chars = stringVal.toCharArray();

        for (int x = 0; x < chars.length; x++)
        {
            char c = chars[x];

            if ((c == 0) && (val != 0))
            {
                c = '\n';
            }
            val += c;
        }

        return val;
    }

    

    public static void main(String[] args)
    {
        List stringsList = new ArrayList();

        stringsList.add("A.1");
        stringsList.add("A.11");
        stringsList.add("A.10");
        stringsList.add("A.20");
        stringsList.add("A.100");
        stringsList.add("A.2");
        stringsList.add("C.2");
        stringsList.add("A.22");
        stringsList.add("A.97");
        stringsList.add("1A.97");

        Collections.sort(stringsList, new StringValueComparator());
        System.out.println("========================================");

    for (Object string : stringsList) {
      System.out.println(string);
    }

        System.out.println("");
    }
}
