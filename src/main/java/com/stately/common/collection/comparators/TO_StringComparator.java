/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.stately.common.collection.comparators;

import java.util.Comparator;

/**
 *
 * @author Edwin
 */
  public class TO_StringComparator implements Comparator<Object>
  {

      private static TO_StringComparator stringComparator = new TO_StringComparator();

      public static TO_StringComparator instance()
      {
          return stringComparator;
      }


        @Override
        public int compare(Object o1, Object o2)
        {
            if(o1 == null)
                o1 = new Object();

            String s1 = o1.toString().trim();
            String s2 = o2.toString().trim();

            int n1=s1.length(), n2=s2.length();
            for (int i1=0, i2=0; i1<n1 && i2<n2; i1++, i2++)
            {
                char c1 = s1.charAt(i1);
                char c2 = s2.charAt(i2);
                if (c1 != c2)
                {
                    c1 = Character.toUpperCase(c1);
                    c2 = Character.toUpperCase(c2);
                    if (c1 != c2)
                    {
                        c1 = Character.toLowerCase(c1);
                        c2 = Character.toLowerCase(c2);
                        if (c1 != c2) {
                            return c1 - c2;
                        }
                    }
                }
            }
            return n1 - n2;
        }
    }
