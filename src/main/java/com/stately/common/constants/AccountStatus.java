/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.common.constants;

import com.stately.common.api.MessageResolvable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author MOB
 */
public enum AccountStatus implements MessageResolvable
{
    APPROVED("Approved", "Approved"),
    UNAPPROVED("UNAPPROVED", "UNAPPROVED");

    private final String label;
    private final String key;

    private AccountStatus(String key, String label)
    {
        this.key = key;
        this.label = label;
    }

    @Override
    public String getCode()
    {
        return this.key;
    }

    @Override
    public String getLabel()
    {
        return this.label;
    }

    public static List<Title> asList()
    {
        return Arrays.asList(Title.values());
    }

    public static List<MessageResolvable> asResolvableList()
    {
        List<MessageResolvable> resolvables = new ArrayList<>();
        resolvables.addAll(asList());
        return resolvables;
    }

    @Override
    public String toString()
    {
        return label;
    }
}
