/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.stately.common.constants;

import com.stately.common.api.EnumCommon;

/**
 *
 * @author Edwin
 */
public enum ActiveInactiveStatus implements EnumCommon
{
    ACTIVE, INACTIVE;

    public String getFullString()
    {
        return getClass().getCanonicalName()+"."+name();
    }
}
