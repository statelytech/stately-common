/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.common.constants;

/**
 *
 * @author Edwin
 */
public interface EnumApi
{    
    public String getLabel();

    public void setLabel(String label);

    public String getKey();

    public void setKey(String key);
    
}
