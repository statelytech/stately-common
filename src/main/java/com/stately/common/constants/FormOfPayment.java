package com.stately.common.constants;

/**
 *
 * @author edwin
 */
public enum FormOfPayment {

    MTN_MOBILE_MONEY("MTN Mobile Money Wallet", "MTN"),
    AIRTEL_MOBILE_MONEY("AIRTEL Mobile Money Wallet", "AIRTEL"),
    MOBILE_MONEY("Mobile Money", "MM"),
    VISA_MASTER_CARD("Visa Master Card", "Visa/Master Card"),
    BANK_PAYMENT("Pay at Bank", "BO"),
    BANKERS_DRAFT("Bankers Draft", "BD"),
    CASH("Cash", "CH"),
    CHEQUE("Cheque", "CHQ"),
    BANK_TRANSFER("Bank Transfer", "BT"),
    CARD_PAYMENT("Card Payment", "Card Payment"),
    INTERNET_BANKING("Internet Banking", "Internet Banking"),
    OTHER("Others", "Other");

    private String initials;
    private String name;

    private FormOfPayment(String name, String code) {
        this.initials = code;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
