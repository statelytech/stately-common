/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.stately.common.constants;

/**
 *
 * @author edwin
 */
 public enum MaritalStatus
{
    SINGLE,MARRIED,DIVORCE,WIDOWED;

    public static MaritalStatus resolve(String value)
    {
        if(value == null)
        {
            return null;
        }
        
        for(MaritalStatus item : MaritalStatus.values())
        {
            if(item.name().equalsIgnoreCase(value)
                    || item.toString().equalsIgnoreCase(value))
            {
                return item;
            }
        }        
        return null;        
    }
}
