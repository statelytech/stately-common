package com.stately.common.constants;

import com.stately.common.api.MessageResolvable;

/**
 *
 * @author Dauda
 */
public enum PaymentPlatform implements MessageResolvable {

    GHIPSS("GHIPSS", "GHIPSS"),
    VISA_CARD_OR_MASTERCARD("Visa/Master Card", "Visa/Master Card"),
    VISA_CARD("Visa Card", "Visa Card"),
    BANK_TRANSFER("Bank Transfer", "Bank Transfer"),
    BANK_PAYMENT("Bank Payment", "Bank Payment"),
    MASTER_CARD("Master Card", "Master Card"),
    MOBILE_MONEY("MTN Mobile Money", "MTN Mobile Money"),
    AIRTEL_MONEY("Airtel Money", "Airtel Money"),
    TIGO_CASH("Tigo Cash", "Tigo Cash"),
    VODDAFONE_CASH("Vodafone Cash", "Vodafone Cash"),
    M_POWER("MPower", "MPower");

    private final String label;
    private final String code;

    private PaymentPlatform(String label, String code) {
        this.label = label;
        this.code = code;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return label;
    }

}
