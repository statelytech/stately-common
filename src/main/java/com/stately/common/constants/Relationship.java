/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.common.constants;

import com.stately.common.utils.StringUtil;

/**
 *
 * @author edwin
 */
public enum Relationship {

    BROTHER("Brother"),
    COUSIN("Cousin"),
    DAUGHTER("Daughter"),
    FAMILY_HEAD("Family Head"),
    FAMILY_MEMBER("Family Member"),
    FATHER("Father"),
    GUARDIAN("Guardian"),
    HUSBAND("Husband"),
    MOTHER("Mother"),
    CHILD("Child"),
    SIBLING("Sibling"),
    SISTER("Sister"),
    SPOUSE("Spouse"),
    SON("Son"),
    UNCLE("Uncle"),
    WIFE("Wife"),
    NEPHEW("Nephew"),
    NIECE("Niece"),
    OTHER("Other");

    String title;

    private Relationship(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static Relationship resolve(String lbl)
    {

        if (StringUtil.isNullOrEmpty(lbl))
        {
            return null;
        }

        Relationship relationship = null;
        for (Relationship l : values())
        {
            if (l.getTitle().equalsIgnoreCase(lbl) || l.name().equalsIgnoreCase(lbl))
            {
                relationship = l;
                break;
            }
        }
        return relationship;
    }
    
    @Override
    public String toString() {
        return title;
    }

}
