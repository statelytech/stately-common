package com.stately.common.constants;

import com.stately.common.api.MessageResolvable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author MOB
 */
public enum RiskClassification implements MessageResolvable
{
    Low("Low", "Low"), 
    Medium ("Medium", "Medium"),
    High ("High", "High");
    
    
    private final String label;
    private final String key;
    
    private RiskClassification(String key, String label)
    {
        this.key = key;
        this.label = label;
    }

    @Override
    public String getCode()
    {
        return this.key;
    }
    
    @Override
    public String getLabel()
    {
        return this.label;
    }

    public static List<RiskClassification> asList()
    {
        return Arrays.asList(RiskClassification.values());
    }
    
    public static List<MessageResolvable> asResolvableList()
    {
        List<MessageResolvable> resolvables = new ArrayList<>();
        resolvables.addAll(asList());
        return resolvables;
    }
    
    @Override
    public String toString()
    {
        return label;
    }
    
    
}
