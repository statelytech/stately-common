package com.stately.common.constants;

import com.stately.common.api.MessageResolvable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author MOB
 */
public enum SourceOfFunding implements MessageResolvable
{
    PERSONAL_SAVING("Personal Saving", "Personal Saving"), 
    INHERITANCE_GIFT ("Inheritance/Gift", "Inheritance/Gift"),
    DIVIDENDS ("Dividends", "Dividends"),
    SALARY ("Salary", "Salary"),
    COMMISSION ("Commission", "Commission"),
    OTHER_INCOME ("Other Income", "Other Income");
    
    
    private final String label;
    private final String key;
    
    private SourceOfFunding(String key, String label)
    {
        this.key = key;
        this.label = label;
    }

    @Override
    public String getCode()
    {
        return this.key;
    }
    
    @Override
    public String getLabel()
    {
        return this.label;
    }

    public static List<SourceOfFunding> asList()
    {
        return Arrays.asList(SourceOfFunding.values());
    }
    
    public static List<MessageResolvable> asResolvableList()
    {
        List<MessageResolvable> resolvables = new ArrayList<>();
        resolvables.addAll(asList());
        return resolvables;
    }
    
    @Override
    public String toString()
    {
        return label;
    }
    
    
}
