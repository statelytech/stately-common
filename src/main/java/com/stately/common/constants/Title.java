package com.stately.common.constants;

import com.stately.common.api.MessageResolvable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Edwin
 */
public enum Title implements MessageResolvable
{
    MR("Mr", "Mr."), 
    MRS ("Mrs", "Mrs."),
    MS ("Ms", "Ms."),
    MASTER ("Master", "Master"),
    MADAM ("Madam", "Madam"),
    MISS ("Miss", "Miss"),
    DR ("Dr", "Dr."),
    REV ("Rev", "Rev."),
    OTHER ("Other", "Other");
    
    
    private final String label;
    private final String key;
    
    private Title(String key, String label)
    {
        this.key = key;
        this.label = label;
    }

    @Override
    public String getCode()
    {
        return this.key;
    }
    
    @Override
    public String getLabel()
    {
        return this.label;
    }
    
    public static Title resolve(String value)
    {
        if(value == null)
        {
            return null;
        }
        
        for(Title item : Title.values())
        {
            if(item.name().equalsIgnoreCase(value)
                    || item.getCode().equalsIgnoreCase(value)
                    || item.getLabel().equalsIgnoreCase(value))
            {
                return item;
            }
        }        
        return null;        
    }

    public static List<Title> asList()
    {
        return Arrays.asList(Title.values());
    }
    
    public static List<MessageResolvable> asResolvableList()
    {
        List<MessageResolvable> resolvables = new ArrayList<>();
        resolvables.addAll(asList());
        return resolvables;
    }
    
    @Override
    public String toString()
    {
        return label;
    }
    
    
}
