/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.common.email;

import com.google.common.base.Strings;
import com.stately.common.utils.CommonUtil;
import com.stately.common.utils.FileUtilities;
import com.stately.common.utils.StringUtil;
import java.io.File;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

/**
 *
 * @author Admin
 */
public class EmailProcessor
{

    private static final Logger LOG = Logger.getLogger(EmailProcessor.class.getName());

    private HtmlEmail htmlEmail;

    private EmailSetupConfig emailCredential;
    private String subject;

    public static boolean SIMULATED_SENDING = false;

    public EmailProcessor()
    {
    }

    public EmailProcessor(EmailSetupConfig emailCredential, String subject)
    {
        this.emailCredential = emailCredential;
        this.subject = subject;

        configureHtmlEmail();
    }

    public EmailProcessor(HtmlEmail htmlEmail, String subject)
    {
        this.htmlEmail = htmlEmail;
        this.subject = subject;

    }

    public HtmlEmail configureHtmlEmail(EmailSetupConfig configuration)
    {
        if (configuration == null)
        {
            return new HtmlEmail();
        }
        this.emailCredential = configuration;

        configureHtmlEmail();

        return htmlEmail;
    }

    public boolean attachToEmail(File fileToAttach)
    {
        return attachToEmail(fileToAttach.getName(), fileToAttach);
    }

    public boolean attachToEmail(String fileName, File fileToAttach)
    {
        EmailAttachment emailAttachment = new EmailAttachment();

        emailAttachment.setDisposition(EmailAttachment.ATTACHMENT);
        emailAttachment.setDescription(fileName);

        try
        {
            if (fileToAttach != null)
            {
                String nameToAttact = fileName + "." + FileUtilities.getFileExtension(fileToAttach.getName());

                emailAttachment.setName(nameToAttact);

                emailAttachment.setPath(fileToAttach.getAbsolutePath());
                htmlEmail.attach(emailAttachment);
                htmlEmail.setBoolHasAttachments(true);
                return true;
            }
        } catch (EmailException ex)
        {
            LOG.log(Level.SEVERE, "Unable to attach file", ex);
        }

        return false;
    }

    public String embedFile(File fileToEmbed, String name)
    {
        try
        {
            if (fileToEmbed != null)
            {
                return htmlEmail.embed(fileToEmbed, name);
            }
        } catch (Exception e)
        {
        }

        return null;
    }

    public String embedFile(URL urlToFile)
    {
        try
        {
            if (urlToFile != null)
            {
                return htmlEmail.embed(urlToFile, "px");
            }
        } catch (Exception e)
        {
        }
        return null;
    }

    public boolean sendHtmlEmail(String receipient, String emailContent)
    {
        try
        {

            if (!CommonUtil.isEmailValid(receipient))
            {
                return false;
            }

            htmlEmail.addTo(receipient);

            setEmailContent(emailContent);

            return sendMail();

        } catch (Exception e)
        {
            LOG.log(Level.SEVERE, "Unable to send file", e);
        }

        return false;
    }

    public boolean setEmailContent(String emailContent)
    {
        try
        {

            if (emailContent == null || emailContent.isEmpty())
            {
                emailContent = "<html></html>";
            }

            htmlEmail.setHtmlMsg(emailContent);

            return true;

        } catch (Exception e)
        {
            LOG.log(Level.SEVERE, "Unable to set email content", e);
        }

        return false;
    }

    public boolean sentHtmlEmail(EmailMessage emailMessage, EmailSetupConfig configuration)
    {
        try
        {
            htmlEmail = configureHtmlEmail(configuration);

            return sentHtmlEmail(emailMessage);

        } catch (Exception e)
        {
            LOG.log(Level.SEVERE, "Unable to send email messaeg", e);
        }

        return false;
    }

    public boolean sentHtmlEmail(EmailMessage emailMessage)
    {
        try
        {

            for (String emailAddress : emailMessage.getToAddressList())
            {
                if (CommonUtil.isEmailValid(emailAddress))
                {
                    htmlEmail.addTo(emailAddress);
                }
            }

            for (String emailAddress : emailMessage.getBcAddressList())
            {
                htmlEmail.addBcc(emailAddress);
            }

            for (com.stately.common.email.EmailAttachment attachemnt : emailMessage.getEmailAttachmentsList())
            {
                attachToEmail(attachemnt.getName(), attachemnt.getFile());
            }

            htmlEmail.setHtmlMsg("<html>" + emailMessage.getEmailText() + "</html>");
            htmlEmail.setSubject(emailMessage.getSubject());

            return sendMail();

        } catch (Exception e)
        {
            LOG.log(Level.SEVERE, "Unable to send email messaeg", e);
        }

        return false;
    }

    public boolean addBccEmail(String emailAddress)
    {
        return addBccEmail(emailAddress, null);

    }
    
    public boolean addBccEmail(String emailAddress, String accountName)
    {
        try
        {
            if (CommonUtil.isEmailValid(emailAddress))
            {

                if (Strings.isNullOrEmpty(accountName))
                {
                    htmlEmail.addBcc(emailAddress);
                } else
                {
                    htmlEmail.addBcc(emailAddress, accountName);
                }

                return true;
            }
        } catch (Exception e)
        {
        }

        return false;
    }

     public boolean addCcEmail(String emailAddress)
    {
        return addCcEmail(emailAddress, null);

    }
    public boolean addCcEmail(String emailAddress, String accountName)
    {
        try
        {
            if (CommonUtil.isEmailValid(emailAddress))
            {

                if (Strings.isNullOrEmpty(accountName))
                {
                    htmlEmail.addCc(emailAddress);
                } else
                {
                    htmlEmail.addCc(emailAddress, accountName);
                }

                return true;
            }
        } catch (Exception e)
        {
        }

        return false;
    }

    public boolean sendHtmlEmail(List<String> receipients, List<String> bccRecepients, String emailContent)
    {
        try
        {

            if (receipients != null)
            {
                for (String receipient : receipients)
                {
                    if (CommonUtil.isEmailValid(receipient))
                    {
                        htmlEmail.addTo(receipient);
                    }
                }
            }

            if (bccRecepients != null)
            {
                for (String bcc : bccRecepients)
                {
                    if (CommonUtil.isEmailValid(bcc))
                    {
                        htmlEmail.addBcc(bcc);
                    }

                }
            }

            htmlEmail.setHtmlMsg(emailContent);

            return sendMail();

        } catch (Exception e)
        {
            LOG.log(Level.SEVERE, "Unable to send file", e);
        }

        return false;
    }
    
    
    public boolean sendHtmlEmail(List<String> receipients, String emailContent)
    {
        return sendHtmlEmail(receipients, Collections.emptyList(), emailContent);
    }

    public boolean sendMail()
    {
        try
        {
            if (SIMULATED_SENDING)
            {
                System.out.println(" Email Simulation send .dispatched : " + LocalDateTime.now() + htmlEmail.getToAddresses());
                return true;
            }

            if (htmlEmail.getToAddresses().isEmpty()
                    && htmlEmail.getBccAddresses().isEmpty()
                    && htmlEmail.getCcAddresses().isEmpty())
            {
                System.out.println("No to Emaill address");
                return false;
            }
            
            if(StringUtil.isNullOrEmpty(htmlEmail.getHostName()))
            {
                System.out.println("No Host Name Defined for sending email");
                return false;
            }

            String emailId = htmlEmail.send();

            System.out.println("Email dispatched : " + emailId + " > to > " + htmlEmail.getToAddresses());

            return true;
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }


    public boolean addTo(String toEmail)
    {

        return addTo(toEmail, null);
    }

    public boolean addTo(List<String> receipientsList)
    {
        try
        {

            if (receipientsList != null)
            {
                for (String receipient : receipientsList)
                {
                    addTo(receipient);
                }
            }

            return true;

        } catch (Exception e)
        {
            LOG.log(Level.SEVERE, "Unable to send file", e);
        }

        return false;
    }

    public boolean addBcc(List<String> receipientsList)
    {
        try
        {

            if (receipientsList != null)
            {
                for (String receipient : receipientsList)
                {
                    addBccEmail(receipient);
                }
            }

            return true;

        } catch (Exception e)
        {
            LOG.log(Level.SEVERE, "Unable to send file", e);
        }

        return false;
    }

    public boolean addTo(String emailAddress, String accountName)
    {
        try
        {
            if (CommonUtil.isEmailValid(emailAddress))
            {

                if (Strings.isNullOrEmpty(accountName))
                {
                    htmlEmail.addTo(emailAddress);
                } else
                {
                    htmlEmail.addTo(emailAddress, accountName);
                }

                return true;
            }
        } catch (Exception e)
        {
        }

        return false;
    }

    public HtmlEmail getHtmlEmail()
    {
        return htmlEmail;
    }

    public static void main(String[] args)
    {
        EmailSetupConfig emailSetupConfig = new DefaultEmailConfiguration();

        String authAddr = "emailer@statelyhub.com";

        String password = "P@$$w0rd";
//        String password = "@B#df54Rt18@";

   emailSetupConfig.setSenderEmailAddress("emailer@statelyhub.com");

        emailSetupConfig.setAuthEmail("emailer@statelyhub.com");
        emailSetupConfig.setEmailHost("mail.statelyhub.com");
        emailSetupConfig.setEmailPassword("py1623PF");


        emailSetupConfig.setUseSsl(true);
        emailSetupConfig.setSmtpPort(465);
        
        
        System.out.println(emailSetupConfig);
        EmailProcessor emailProcessor = new EmailProcessor(emailSetupConfig, "My Email- Testing :: " + LocalDateTime.now());

//        emailProcessor.sendHtmlEmail("clientservice@icsecurities.com", "Sample Email1 - " + LocalDateTime.now());
         emailProcessor.sendHtmlEmail("edwin.amoakwa@gmail.com", "Sample Email1 - " + LocalDateTime.now());
//         emailProcessor.sendHtmlEmail("edwin.amoakwa@gmail.com", "Sample Email1 - " + LocalDateTime.now());

    }

    private void configureHtmlEmail()
    {
        htmlEmail = new HtmlEmail();

        try
        {
            System.out.println(((DefaultEmailConfiguration) emailCredential).info());
        } catch (Exception e)
        {
        }

        try
        {
            htmlEmail.setHostName(emailCredential.getEmailHost());
            htmlEmail.setFrom(emailCredential.getSenderEmailAddress(), emailCredential.getAccountName());
            htmlEmail.setSubject(subject);
            htmlEmail.setAuthentication(emailCredential.getAuthEmail(), emailCredential.getEmailPassword());

            
            
            if (emailCredential.getSmtpPort() != 0)
            {
                htmlEmail.setSmtpPort(emailCredential.getSmtpPort());
            }

            if (emailCredential.isUseSsl())
            {
//                htmlEmail.sets
//                htmlEmail.setSmtpPort(emailCredential.getSmtpPort());
//                htmlEmail.setSslSmtpPort("" + emailCredential.getSmtpPort());
//                htmlEmail.setSSLOnConnect(true);
                htmlEmail.setStartTLSEnabled(true);
                htmlEmail.setSSLOnConnect(true);
            }

            htmlEmail.getMailSession().getProperties().put("mail.smtp.ssl.trust", emailCredential.getEmailHost());
            System.out.println(htmlEmail.getMailSession().getProperties());

            if (true)
            {
                return;
            }

            htmlEmail = new HtmlEmail();
            htmlEmail.getMailSession().getProperties().put("mail.smtp.ssl.trust", emailCredential.getEmailHost());

//htmlEmail.getMailSession().getProperties().put("mail.smtps.auth", "true");
            htmlEmail.getMailSession().getProperties().put("mail.debug", "true");
            htmlEmail.getMailSession().getProperties().put("mail.smtps.port", emailCredential.getSmtpPort());

            htmlEmail.getMailSession().getProperties().put("mail.smtps.socketFactory.port", emailCredential.getSmtpPort());

            htmlEmail.getMailSession().getProperties().put("mail.smtps.socketFactory.fallback", "false");

            if (emailCredential.isUseSsl())
            {
                htmlEmail.getMailSession().getProperties().put("mail.smtp.starttls.enable", "true");
                htmlEmail.getMailSession().getProperties().put("mail.smtps.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            }

//htmlEmail.setSmtpPort(emailCredential.getSmtpPort());
//htmlEmail.setStartTLSEnabled(true);
            System.out.println(htmlEmail);
            System.out.println(htmlEmail.getMailSession().getProperties());
//            htmlEmail.
//            htmlEmail = new  HtmlEmail()
        } catch (Exception e)
        {
            LOG.log(Level.SEVERE, "Email configuration error", e);
        }
    }

}
