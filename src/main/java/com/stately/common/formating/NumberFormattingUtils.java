/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.stately.common.formating;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 *
 * @author Edwin
 */
public class NumberFormattingUtils {

    


    private static final NumberFormat numberFormat = NumberFormat.getInstance();
    private static final DecimalFormat decimalFormat = new DecimalFormat();

    private static final String DECIMAL_PATTERN = "###.##";
//    public static

    
    public static String formatNumber(Number number, String pattern)
    {
        decimalFormat.applyPattern(pattern);
        return decimalFormat.format(number);
    }
    
    
    public static String formatNumber(long number, int numofdigs)
    {
        numberFormat.setMinimumIntegerDigits(numofdigs);
        return numberFormat.format(number);
    }

    public static String formatNumber(String number, int numofdigs)
    {
        numberFormat.setMinimumIntegerDigits(numofdigs);
        return numberFormat.format(Integer.parseInt(number));
    }

    public static String getFormatedAmount(double number)
    {
        numberFormat.setMaximumFractionDigits(2);
        numberFormat.setMinimumFractionDigits(2);
        return numberFormat.format(number);
    }

    public static String formatDouble(double number, int numberOfDigit)
    {
        numberFormat.setMaximumFractionDigits(numberOfDigit);
        numberFormat.setMinimumFractionDigits(numberOfDigit);
        return numberFormat.format(number);

    }

    public static Double formatDoubleNum(double number, int numberOfDigit)
    {
        DecimalFormat df = new DecimalFormat();
        
        df.setMaximumFractionDigits(numberOfDigit);
        df.setMinimumFractionDigits(numberOfDigit);
//        numberFormat.setm

//df.setRoundingMode(RoundingMode.HALF_UP);
//        df.applyPattern("#,###");

//        System.out.println(df.getRoundingMode());
//        System.out.println(df.getMaximumFractionDigits());
//        System.out.println(df.getMinimumFractionDigits());
//        System.out.println(" ------ " + df.format(number));
        String formatedNumber =  df.format(number).replace(",", "");

        return Double.parseDouble(formatedNumber);

    }

    public static Double formatDecimalNumberTo_2(double number)
    {
        decimalFormat.applyPattern(DECIMAL_PATTERN);
        
        String numberString =  decimalFormat.format(number);

        return Double.parseDouble(numberString);
    }


//    public static String formatDecimalNumberTo_2(double number)
//    {
//        decimalFormat.applyPattern(DECIMAL_PATTERN);
//
//        String numberString =  decimalFormat.format(number);
//
//        return Double.parseDouble(numberString);
//    }

    public static String formatNumberAsPosition(int number)
    {

        if(number == 0)
            return "";

        String numberString = Integer.toString(number);
        
        String formattedString = "";

        if(numberString.equalsIgnoreCase("11"))
        {
            formattedString = number + "th";
        }
        else if(numberString.equalsIgnoreCase("12"))
        {
            formattedString = number + "th";
        }
        else if(numberString.equalsIgnoreCase("13"))
        {
            formattedString = number + "th";
        }
        else if(numberString.endsWith("1"))
        {
            formattedString = number + "st";
        }
        else if(numberString.endsWith("2"))
        {
            formattedString = number + "nd";
        }
        else if(numberString.endsWith("3"))
        {
            formattedString = number + "rd";
        }
        else
            formattedString = number + "th";
       
        return formattedString;
    }

    
    public static void main(String[] args)
    {
         System.out.println(formatDoubleNum(32.5, 0));
         
         System.out.println(Math.rint(32.5));
         DecimalFormat df = new DecimalFormat();
         df.setRoundingMode(RoundingMode.HALF_UP);
         df.setMinimumFractionDigits(0);
         df.setMaximumFractionDigits(0);
         System.out.println(df.format(32.5));
    }
    
}
