/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.common.formating;

import com.stately.common.utils.DateTimeUtils;
import com.stately.common.utils.StringUtil;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author Edwin
 */
public class ObjectValue {

    public static String getStringValue(Object object) 
    {
        if (object == null) {
            return "";
        } else {
            return object.toString().trim();
        }
    }

//    public static String get_AAAAAAAstringValue(Object object) {
//        if (object == null) {
//            return "";
//        } else {
//            return object.toString();
//        }
//    }

    public static Double getDoubleValue(Object object) {
        if (object == null) {
            return null;
        }

        try {
            return Double.parseDouble(String.valueOf(object.toString().replaceAll(",", "")));
        } catch (Exception e) {
        }
        return null;
    }

    public static double get_doubleValue(Object object) {
        if (object == null) {
            return 0.0;
        } else {
            try {
                return new BigDecimal(object.toString().replaceAll(",", "").trim()).doubleValue();
            } catch (Exception e) {
            }
        }
        return 0.0;
    }

    public static Integer getIntegerValue(Object object) {

        if (object == null) {
            return null;
        } else {
            try {
                String data = object.toString().replaceAll(",", "");
                data = StringUtil.removeTraillingZero(data);

//                    if (data.contains("."))
//                    {
                        data = data.replaceAll("\\.0", "");
//                    }
                try {
                    return Integer.parseInt(data);
                } catch (Exception e) {
                }
            } catch (Exception e) {
//                    e.printStackTrace();
            }
        }

        return null;

    }

    

    public static Object getCleanValue(Object object) {

        if (object == null) {
            return null;
        } else {
            try {
                String data = object.toString().replaceAll(",", "");

//                    if (data.contains("."))
//                    {
//                        data = data.replaceAll("\\.0", "");
//                    }
                return data;
            } catch (Exception e) {
//                    e.printStackTrace();
            }
        }

        return null;

    }

    
    
    public static int get_intValue(Object object) {

        if (object == null) {
            return 0;
        } else {
            Integer integer = getIntegerValue(object);
            if (integer != null) {
                return integer;
            }
        }

        return 0;
    }

    public static long get_longValue(Object object) {

        if (object == null) {
            return 0;
        } else {
            
            String data = (String) getCleanValue(object);
            data = data.replaceAll("\\.0", "");
            Long longValue = Long.parseLong(data);
            if (longValue != null) {
                return longValue;
            }
        }

        return 0;
    }
    
    
    public static LocalDate getLocalDate(Object object)
    {
        if (object == null)
        {
            return null;
        }

        try
        {
            if (object instanceof LocalDate)
            {
                LocalDate valueDate = (LocalDate) object;
                return valueDate;
            }

        } catch (Exception e)
        {
            System.out.println(e.getMessage() + " ===  unable to process " + object + " as LocalDate");
//            e.printStackTrace();
        }

        try
        {
            if (object instanceof Date)
            {
                Date date = (Date) object;
                LocalDate valueDate = DateTimeUtils.toLocalDate(date);

                if (valueDate != null)
                {
                    return valueDate;
                }
            }

        } catch (Exception e)
        {
            System.out.println(e.getMessage() + " ===  unable to process " + object + " as Utill Date");
//            e.printStackTrace();
        }

        try
        {
            String dateStr = object.toString().trim().replaceAll(" ", "");

            Date date = DateTimeUtils.parseDate(dateStr, "dd/MM/yyyy");
            LocalDate valueDate = DateTimeUtils.toLocalDate(date);

            return valueDate;
        } catch (Exception e)
        {
            System.out.println("unable to process " + object + " as date - ");
//            e.printStackTrace();
        }

        return null;
    }

    public static void main(String[] args) {
        System.out.println("texting data parsing ... ");
        System.out.println(Double.parseDouble("2.8385499E7") + 1);
        System.out.println(get_intValue("2.8385499E7"));
    }
}
