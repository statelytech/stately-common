package com.stately.common.utils;

import com.google.common.base.Strings;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Ainoo Dauda
 * @company IC Securities Ghana Ltd
 * @email dauda.ainoo@icsecurities.com
 * @date 20 October 2015
 *
 *
 */
public class HttpConnectionUtils
{

    public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
    static 
    {
        SslOveride.init();
    }
    
    public static boolean log = true;
    
    public static HttpConResponse sendHttp(String url, String method, String contentType, String body)
    {
        return sendHttp(url, method, contentType, body, null);
    }
    
    
    public static HttpConResponse sendHttp(String url, String method, String contentType, String body, Map<String,String> headers)
    {
        HttpConResponse httpConResponse = new HttpConResponse();
        try
        {
            
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod(method);
            con.setRequestProperty("User-Agent", USER_AGENT);
            
            if(contentType != null)
            {
                con.setRequestProperty("Content-Type", contentType+";charset=UTF-8");
            }
            
            if(headers != null)
            {
                for (Map.Entry<String, String> entry : headers.entrySet())
                {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    con.setRequestProperty(key, value);
                }
            }
            
//            System.out.println("Printing request properties ...");
//            System.out.println(con.getRequestProperties() +"\n");
           
            con.setConnectTimeout(50000);
            con.setReadTimeout(100000);
//            
//            con.setConnectTimeout(5000);
//            con.setReadTimeout(10000);

            // Send post request
            if(null != body)
            {
                con.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.write(body.getBytes());
                wr.flush();
                wr.close();
            }

            
            int responseCode = con.getResponseCode();
            httpConResponse.setResponseCode(responseCode);
            String responseBody = "";
            
            if(log)
            {
                System.out.println("\nSending '" + method + "' request to URL : " + url);
                System.out.println("data sent in body: " +  body);
                System.out.println("Response Code : " + responseCode);
            }
       
            InputStream inputStream = null;

            try
            {
                inputStream = con.getInputStream();
            } catch (Exception e)
            {
                inputStream = con.getErrorStream();
            }
            if(inputStream == null)
            {
                httpConResponse.setResponse("InputStream Reading Error");
                return httpConResponse;
            }

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(inputStream));
            String inputLine = null;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null)
            {
                responseBody = response.append(inputLine).toString();
            }
            in.close();
            
             if(log)
            {
                System.out.println("Response Body : "+ responseBody);
            }

                
            //print result
//            System.out.println("The Response >>>>>> " + respd);
            httpConResponse.setResponse(responseBody);
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
            if(log)
            {
                e.printStackTrace();
            }
        }

        return httpConResponse;
    }
    
    
    public static HttpConResponse sendJsonPost(String url, String body)
    {
        return sendHttp(url, "POST", "application/json", body);
    }

    public static HttpConResponse sendJsonPost(String url, String body, Map<String,String> headers)
    {
        return sendHttp(url, "POST", "application/json", body, headers);
    }

    public static HttpConResponse sendJsonGet(String url, String body, Map<String,String> headers)
    {
        return sendHttp(url, "GET", "application/json", body, headers);
    }

    public static HttpConResponse sendGet(String url, Map<String,String> headers)
    {
        return sendHttp(url, "GET", "application/json", null, headers);
    }
    
//    public static HttpConResponse sendJsonGet(String url, String body)
//    {
//        return sendHttp(url, "GET", "application/json", body);
//    }

    public static HttpConResponse sendXmlPost(String url, String body)
    {
        return sendHttp(url, "POST", "application/xml", body);
    }

    public static HttpConResponse sendPost(String url)
    {
        return sendHttp(url, "POST", null, null);
    }

    public static HttpConResponse sendGet(String url)
    {
        return sendHttp(url, "GET", null, null);
    }

    public static boolean fileExists(String file)
    {
        try
        {
            final URL url = new URL(file);
            HttpURLConnection huc = (HttpURLConnection) url.openConnection();
            huc.setRequestMethod("HEAD");
            int responseCode = huc.getResponseCode();

            if (responseCode != 404)
            {
                System.out.println("exist >>> " + url);
                return true;
            } else
            {
                System.out.println("Doesn't Exist >>> " + url);
//System.out.println("BAD");
            }
        } catch (Exception e)
        {
            System.out.println("cannot check the existance of " + file);
            e.printStackTrace();
        }

        return false;
    }
    
    public static String encodeParamValues(Map<String, String> map)
    {
        String webParam = "";
        try
        {
            List<String> list = new LinkedList<>(map.keySet());

            for (Iterator<String> iterator = list.iterator(); iterator.hasNext();)
            {
                String param = iterator.next();
                
                if(Strings.isNullOrEmpty(param))
                {
                    param = "";
                }
                
                webParam += param + "=" + URLEncoder.encode(map.get(param), "UTF-8");

                if (iterator.hasNext())
                {
                    webParam += "&";
                }

            }

        } catch (UnsupportedEncodingException ex)
        {
            ex.printStackTrace();
        }

        return webParam;
    }
    
    public static void main(String[] args)
    {
        System.out.println(fileExists("https://download.cnet.com/audio-media-players/android/"));
        System.out.println(fileExists("http://139.162.171.157:8080/mtnsdpswitch/rest/bib/subscriber-detail?phoneNo=233245184371"));
        //System.out.println(sendGet("http://139.162.171.157:8080/mtnsdpswitch/rest/bib/subscriber-detail?phoneNo=233245184371"));
//        System.out.println(sendHttp("http://139.162.171.157:8080/mtnsdpswitch/rest/bib/subscriber-detail?phoneNo=233245184371","GET",null, null));
//        System.out.println(sendHttp("http://localhost/iexchange-unity/rest/api/investor-update?","POST",null, null));
//        System.out.println(sendHttp("http://localhost/iexchange-unity/rest/api/investor-update?","POST",MediaType.APPLICATION_JSON, "{'phoneNo':'0245184371'}"));
           Map<String, String> map = new LinkedHashMap<>();
           map.put("Authorization", "Basic eWdkeGpxY2c6YWtnZm5oemU=");
            HttpConResponse conResponse = sendJsonPost("https://api.hubtel.com/v1/merchantaccount/merchants/HM0709170030/receive/mobilemoney", "{}", map);
            System.out.println(conResponse.getResponseCode());
            System.out.println(conResponse.getResponse());
            
    }

}
