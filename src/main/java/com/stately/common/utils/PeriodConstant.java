/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.stately.common.utils;

/**
 *
 * @author Edwin
 */
public enum PeriodConstant
{    
    FROM_INCEPTION("From Inception"),
    LAST_MONTH("Last Month"),
    A_MONTH_AGO("A month ago"),
    SIX_MONTHS_AGO("Six month ago"),
    THREE_MONTHS_AGO("Three months ago"),
    TWELVE_MONTHS_AGO("A year ago"),
    YEAR_TO_DATE("Year to Date");
    
    
    private final String label;

    private PeriodConstant(String label)
    {
        this.label = label;
    }

    @Override
    public String toString()
    {
        return label; 
    }
    
    
    
}
