/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.stately.common.utils;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Edwin
 * @param <T>
 */
public class ProcessingMessage<T>
{
    private boolean successfull;
    private String responseCode;
    
    private T result;
    
    public static enum Severity{
        INFO, FATAL, WARN
    }
    
    List<Message> messagesList = new LinkedList<>();

    public ProcessingMessage(String msg, Severity type)
    {
        messagesList.add(new Message(msg, type));
    }

    public ProcessingMessage()
    {
    }
    
    
    public ProcessingMessage chain(ProcessingMessage message)
    {
        if(isSuccessfull()== false || message.isSuccessfull() == false)
        {
            setSuccessfull(false);
        }
        messagesList.addAll(message.getMessagesList());
        
        return this;
    }

    public void addProcessingMessage(String msg, Severity type)
    {
        Message message = new Message(msg, type);
        messagesList.add(message);
    }

    public void info(String msg)
    {
        Message message = new Message(msg, Severity.INFO);
        messagesList.add(message);
    }
    
    public void error(String msg)
    {
        Message message = new Message(msg, Severity.FATAL);
        messagesList.add(message);
    }
    
    public void addWarn(String msg)
    {
        Message message = new Message(msg, Severity.WARN);
        messagesList.add(message);
    }

//    public String getMsg()
//    {
//        return msg;
//    }
//
//    public void setMsg(String msg)
//    {
//        this.msg = msg;
//    }
//
//    public Severity getType()
//    {
//        return type;
//    }
//
//    public void setType(Severity type)
//    {
//        this.type = type;
//    }

    public List<Message> getMessagesList()
    {
        return messagesList;
    }

    public void setMessagesList(List<Message> messagesList)
    {
        this.messagesList = messagesList;
    }

    
    public boolean isSuccessfull()
    {
        return successfull;
    }

    public void setSuccessfull(boolean successfull)
    {
        this.successfull = successfull;
//        this.state = successfull;
    }

//    public Boolean getState()
//    {
//        return state;
//    }

    public T getResult()
    {
        return result;
    }

    public void setResult(T result)
    {
        this.result = result;
    }

    

    public static class Message
    {
        private String msg;
        private Severity type;
        
        public Message(String msg, Severity type)
    {
        this.msg = msg;
        this.type = type;
    }

        public String getMsg()
        {
            return msg;
        }

        public void setMsg(String msg)
        {
            this.msg = msg;
        }

        public Severity getType()
        {
            return type;
        }

        public void setType(Severity type)
        {
            this.type = type;
        }

        @Override
        public String toString() {
            return "Message{" + "msg=" + msg + ", type=" + type + '}';
        }
        
        
    }

    @Override
    public String toString()
    {
        return "ProcessingMessage{" + "successfull=" + successfull + ", result=" + result + ", messagesList=" + messagesList + '}';
    }
    
    
    
    
}
