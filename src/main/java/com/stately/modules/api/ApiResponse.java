/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.modules.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Edwin
 */
public class ApiResponse
{
    private boolean success;
    private int statusCode;
    private String message;
    private Object data;
    private List<ErrorModel> errors;
    
    
    public static Response build(ApiResponse apiResponse) 
    {
//        return ResponseEntity.status(apiResponse.getStatusCode())
//                .body(apiResponse);
        
        
        
        if(!apiResponse.isSuccess())
        {
            if (apiResponse.getErrors() != null)
            {
                if (!apiResponse.getErrors().isEmpty())
                {
                    if (apiResponse.getErrors().size() == 1)
                    {
                        apiResponse.setMessage(apiResponse.getErrors().get(0).getMessage());
                        return Response.status(apiResponse.getStatusCode())
                                .type(MediaType.APPLICATION_JSON)
                                .entity(apiResponse.toJson()).build();
                    }

                    return Response.status(apiResponse.getStatusCode())
                            .type(MediaType.APPLICATION_JSON)
                            .entity(apiResponse.getErrors()).build();
                }
            }
        }
            
            
            return Response.status(apiResponse.getStatusCode())
                .entity(apiResponse.toJson()).build();
//            
////            if(!apiResponse.getErrors().isEmpty())
////            {
//                return ResponseEntity.status(apiResponse.getStatusCode())
//                        .contentType(MediaType.APPLICATION_JSON)
////                .body(apiResponse.getErrors());
//                .body(apiResponse);
////            }
//        }
        
//        if(apiResponse.isSuccess())
//        {
//            if(apiResponse.getData() == null)
//            {
//                return ResponseEntity.status(apiResponse.getStatusCode())
//                .contentType(MediaType.APPLICATION_JSON)
////                .body(new Gson().toJson(apiResponse.getData()));
//                .body(apiResponse.toJson());
//            }else
//            {
//                return ResponseEntity.status(apiResponse.getStatusCode())
//                .contentType(MediaType.APPLICATION_JSON)
////                .body(new Gson().toJson(apiResponse.getData()));
//                .body(apiResponse.getData());
//            }
//        }
//        
//        return ResponseEntity.status(apiResponse.getStatusCode())
//                .contentType(MediaType.APPLICATION_JSON)
//                .body(new Gson().toJson(apiResponse));
////                .body(new Gson().toJson(apiResponse.getData()));
////                .body(apiResponse.getData());
    }
    
    public static Response delete(boolean status)
    {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setSuccess(status);
        
        if(status)
        {
            apiResponse.setMessage("Deleted");
            apiResponse.setStatusCode(Response.Status.OK.getStatusCode());
        }
        else
        {
            apiResponse.setMessage("Unable to delete, or doesn't exist");
            apiResponse.setStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
        }
//        apiResponse.setData(object);
        
        return build(apiResponse);
    }
    
    public static Response delete(boolean status, Object object)
    {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setSuccess(status);
        
        if(status)
        {
            apiResponse.setMessage("Item Deleted");
            apiResponse.setStatusCode(Response.Status.OK.getStatusCode());
        }
        else
        {
            apiResponse.setMessage("Unable to delete, or doesn't exist");
            apiResponse.setStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
        }
        apiResponse.setData(object);
        
        return build(apiResponse);
    }
    
    public static Response cantFind(String id, Object entity)
    {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setSuccess(false);
        apiResponse.setStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
        
        if (entity == null)
        {
            apiResponse.setMessage("Data not found !!!");
        } else
        {
            apiResponse.setData(entity);
            apiResponse.setMessage("Cannot find item with ID - " + id);
        }

        return build(apiResponse);
    }
     
     public static Response findResult(Object object)
    {
        ApiResponse apiResponse = new ApiResponse();

        if (object == null)
        {
            apiResponse.setSuccess(false);
            apiResponse.setStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
            apiResponse.setMessage("Data not found !!!");
        } else
        {
            apiResponse.setSuccess(true);
            apiResponse.setData(object);
            apiResponse.setStatusCode(Response.Status.OK.getStatusCode());
            apiResponse.setMessage("Data found");
        }

        return build(apiResponse);
    }
     
    
       public static Response updatedResult(boolean status)
    {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setSuccess(status);
        
        if(status)
        {
            apiResponse.setMessage("Updated");
        }
        else
        {
            apiResponse.setMessage("Unable to Update");
        }
//        apiResponse.setData(object);
        apiResponse.setStatusCode(Response.Status.OK.getStatusCode());
        return build(apiResponse);
    }
    
       public static Response updatedResult(boolean status,Object object)
    {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setSuccess(status);
        
        if(status)
        {
            apiResponse.setMessage("Updated");
        }
        else
        {
            apiResponse.setMessage("Unable to Update");
        }
        apiResponse.setData(object);
        apiResponse.setStatusCode(Response.Status.OK.getStatusCode());
        return build(apiResponse);
    }
    
    public void addError(String code, String message)
    {
//        err
    }
    public Response toResponse()
    {
        return Response.status(statusCode)
                .entity(this)
                .build();
    }
    
    public static ApiResponse successfull()
    {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setSuccess(true);
        return apiResponse;
    }
    
    public static ApiResponse failed()
    {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setSuccess(true);
        return apiResponse;
    }
    
//    public static Response failed()
//    {
//        ApiResponse apiResponse = new ApiResponse();
//        apiResponse.setSuccess(true);
//        return apiResponse;
//    }
    
    public static Response badRequest(ApiResponse apiResponse) 
    {
        return Response.status(apiResponse.getStatusCode())
                .entity(apiResponse.toJson())
                .build();
    }
    
    public static Response badRequest(String message) 
    {
        ApiResponse response = new ApiResponse();
        response.setMessage(message);
        response.setStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
        response.setSuccess(false);
        return Response.status(Response.Status.BAD_REQUEST).entity(response.toJson()).build();
    }
    
    public static Response info(String message, Object object) 
    {
        ApiResponse response = new ApiResponse();
        response.setMessage(message);
        response.setStatusCode(Response.Status.OK.getStatusCode());
        response.setSuccess(true);
        
        if(object != null)
        {
            response.setData(object);
        }
        
        return build(response);
        
//        return Response.status(Response.Status.OK).entity(response.toJson()).build();
    }
    
    public static Response internalServerError(String message) 
    {
        ApiResponse response = new ApiResponse();
        response.setMessage(message);
        response.setStatusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
        response.setSuccess(false);
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(response.toJson()).build();
    }
    
    public static Response error() 
    {
        ApiResponse response = new ApiResponse();
        response.setMessage("Error Occured");
        response.setStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
        response.setSuccess(false);
        return Response.status(Response.Status.BAD_REQUEST).entity(response.toJson()).build();
    }
    
    public static Response error(String message) 
    {
        ApiResponse response = new ApiResponse();
        response.setMessage(message);
        response.setStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
        response.setSuccess(false);
        return Response.status(Response.Status.BAD_REQUEST).entity(response.toJson()).build();
    }
    
    public static Response error(List<ErrorModel>  errosList) 
    {
        ApiResponse response = new ApiResponse();
        response.setMessage("Error in processing");
        response.setErrors(errosList);
        response.setStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
        response.setSuccess(false);
        return Response.status(Response.Status.BAD_REQUEST).entity(response.toJson()).build();
    }
    
    public static ApiResponse created()
    {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setSuccess(true);
        apiResponse.setStatusCode(Response.Status.CREATED.getStatusCode());
        return apiResponse;
    }
        
    public static Response created(Object object)
    {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setSuccess(true);
        apiResponse.setData(object);
        apiResponse.setStatusCode(Response.Status.CREATED.getStatusCode());
        return Response.status(Response.Status.CREATED).entity(apiResponse).build();
    }
    public static Response ok(Object object)
    {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setSuccess(true);
        apiResponse.setData(object);
        apiResponse.setStatusCode(Response.Status.OK.getStatusCode());
        return Response.status(Response.Status.OK).entity(apiResponse.toJson()).build();
    }
    
    public static Response ok(String message)
    {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setSuccess(true);
        apiResponse.setMessage(message);
        apiResponse.setStatusCode(Response.Status.OK.getStatusCode());
        return Response.status(Response.Status.OK).entity(apiResponse.toJson()).build();
    }
    
        
    public ApiResponse withData(Object data)
    {
        this.data = data;
        return this;
    }

    public List<ErrorModel> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorModel> errors) {
        this.errors = errors;
    }
    
    public boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(boolean success)
    {
        this.success = success;
    }

    public int getStatusCode()
    {
        return statusCode;
    }

    public void setStatusCode(int statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public Object getData()
    {
        return data;
    }

    public void setData(Object data)
    {
        this.data = data;
    }
    
    
    
    public String toJson()
    {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(LocalDate.class, new LocalDateSerializer())
                .registerTypeAdapter(LocalDate.class, new LocalDateDeserializer())
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer())
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeDeserializer())
                .registerTypeAdapter(LocalDate.class, new LocalDateAdapter().nullSafe())
                .create();
        return gson.toJson(this);
    }
    
    public static Gson getGson()
    {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(LocalDate.class, new LocalDateSerializer())
                .registerTypeAdapter(LocalDate.class, new LocalDateDeserializer())
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer())
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeDeserializer())
                .registerTypeAdapter(LocalDate.class, new LocalDateAdapter().nullSafe())
                .create();
        return gson;
    }

    static class LocalDateSerializer implements JsonSerializer<LocalDate>
    {
        @Override
        public JsonElement serialize(LocalDate date, Type typeOfSrc, JsonSerializationContext context)
        {
            return new JsonPrimitive(date.format(DateTimeFormatter.ISO_LOCAL_DATE)); // "yyyy-mm-dd"
        }
    }

    static class LocalDateTimeSerializer implements JsonSerializer<LocalDateTime>
    {
        private static final DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        @Override
        public JsonElement serialize(LocalDateTime localDateTime, Type srcType, JsonSerializationContext context)
        {
            return new JsonPrimitive(formatter.format(localDateTime));
        }
    }

    static class LocalDateDeserializer implements JsonDeserializer<LocalDate>
    {
        @Override
        public LocalDate deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException
        {
            return LocalDate.parse(json.getAsString(),
                    DateTimeFormatter.ISO_LOCAL_DATE.withLocale(Locale.ENGLISH));
        }
    }

    static class LocalDateTimeDeserializer implements JsonDeserializer< LocalDateTime>
    {
        @Override
        public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException
        {
            return LocalDateTime.parse(json.getAsString(),
                    DateTimeFormatter.ISO_LOCAL_DATE_TIME.withLocale(Locale.ENGLISH));
        }
    }
    
    public static class LocalDateAdapter extends TypeAdapter<LocalDate> {
    @Override
    public void write( final JsonWriter jsonWriter, final LocalDate localDate ) throws IOException {
        jsonWriter.value(localDate.toString());
    }

    @Override
    public LocalDate read( final JsonReader jsonReader ) throws IOException {
        return LocalDate.parse(jsonReader.nextString());
    }
}
    
}
