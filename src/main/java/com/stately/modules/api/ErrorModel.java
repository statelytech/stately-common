/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.modules.api;

/**
 *
 * @author Edwin
 */
public class ErrorModel
{
    private String field;
    private String message;
    private Object invalidValue;

    public ErrorModel()
    {
    }
    
    public ErrorModel(String message)
    {
        this.message = message;
    }
    
    public ErrorModel(String field, String message)
    {
        this.field = field;
        this.message = message;
    }
    
    public String getField()
    {
        return field;
    }

    public void setField(String field)
    {
        this.field = field;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public Object getInvalidValue()
    {
        return invalidValue;
    }

    public void setInvalidValue(Object invalidValue)
    {
        this.invalidValue = invalidValue;
    }
    
}
