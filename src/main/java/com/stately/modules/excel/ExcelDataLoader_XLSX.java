/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.modules.excel;

import com.stately.common.utils.StringUtil;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 *
 * @author Edwin
 */
public class ExcelDataLoader_XLSX implements ExcelLoader {

    @Override
    public ExcelWorkBook read(String fileName) {
        System.out.println("reading data");

        File excellFile = new File(fileName);
        ExcelWorkBook excelWorkBook = new ExcelWorkBook();
        excelWorkBook.setFileName(excellFile.getName());

        System.out.println("file to process is " + excellFile);
        try {

            Workbook workbook = WorkbookFactory.create(new File(fileName));

//            Workbook workbook = Workbook.getWorkbook(excellFile);
            System.out.println("file loaded. ");

//            Sheet[] excelSheets = workbook.getSheets();
            Iterator<Sheet> sheetIterator = workbook.sheetIterator();
            
            while (sheetIterator.hasNext()) {
                Sheet sheet = sheetIterator.next();
//            System.out.println("=> " + sheet.getSheetName());

                ExcelSheet excelSheet = read(sheet);
                excelWorkBook.addExcelSheet(excelSheet);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return excelWorkBook;
    }

    @Override
    public ExcelWorkBook read(String fileName, String sheetName) {
        System.out.println("reading data");

        File excellFile = new File(fileName);
        ExcelWorkBook excelWorkBook = new ExcelWorkBook();
        excelWorkBook.setFileName(excellFile.getName());

        System.out.println("file to process is " + excellFile);
        try {

            Workbook workbook = WorkbookFactory.create(new File(fileName));

            System.out.println("file loaded. ");

            Sheet sheet = workbook.getSheet(sheetName);

            System.out.print("processing sheet " + sheet.getSheetName());
            ExcelSheet excelSheet = read(sheet);
            excelWorkBook.addExcelSheet(excelSheet);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return excelWorkBook;
    }

    public ExcelSheet read(Sheet sheet) {
        System.out.println("Processing sheet ..... " + sheet.getSheetName());
        ExcelSheet excelSheet = new ExcelSheet();

        System.out.print("processing sheet " + sheet.getSheetName());
        excelSheet.setSheetName(sheet.getSheetName());

//        sheet.getr
        //determine Column counts by headers
        int totalCols = 0;
        for (Row row : sheet) {
//            row.
            for (Cell cell : row) {
                totalCols++;
            }
            break;
        }

        System.out.println("Total Colums Identified : " + totalCols);

        for (Row row : sheet) {
            excelSheet.createNewRows();

//            row.get
            for (int i = 0; i < totalCols; i++) {
                Cell cell = row.getCell(i);

                excelSheet.addRowData(readCellValue(cell));

            }

            
            excelSheet.completeRows();
        }

        return excelSheet;

    }

    public Object readCellValue(Cell cell) {

        if(cell == null)
        {
            return "";
        }
        
        if(cell.getCellType() == CellType.FORMULA)
        {
            return readCellValue(cell, cell.getCachedFormulaResultType());
        }
        return readCellValue(cell, cell.getCellType());

    }

    

    public Object readCellValue(Cell cell, CellType cellType) {

        Object cellValue = "";

        if (cellType == null) {
            cellValue = cell.getStringCellValue();
            return cellValue;
        }

        switch (cellType) {
            case STRING:
                cellValue = cell.getStringCellValue();
                break;
            case BOOLEAN:
                cellValue = cell.getBooleanCellValue();
                break;
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    cellValue = cell.getDateCellValue();
                } else {
                    cellValue = cell.getNumericCellValue();
                }
                break;
            case ERROR:
                            try {
                cellValue = String.valueOf(cell.getErrorCellValue());
            } catch (Exception e) {
            }
            break;
            default:
                            try {
                cellValue = cell.getStringCellValue();
            } catch (Exception e) {
                e.printStackTrace();
            }

            break;
        }

        return cellValue;

    }

    
    
    public static void main(String[] args) {
        try {
//            List<Object[]> list = new ExcelDataLoader_XLSX().read("C:\\Users\\Edwin\\Downloads\\AR.xlsx").getWorkBookData();
            List<Object[]> list = new ExcelDataLoader_XLSX().read("d:/tmp/file.xlsx").getWorkBookData();
//            List<Object[]> list = new ExcelDataLoader_XLSX().read("C:/Users/Edwin/Downloads/Opinamangdraft.xlsx").getWorkBookData();

            StringUtil.printObjectListArray(list);

        } catch (Exception e) {
        }
    }
}
