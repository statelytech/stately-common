/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.modules.excel;

import com.stately.common.formating.ObjectValue;
import com.stately.common.model.DateRange;
import com.stately.common.reflect.ClassInfo;
import com.stately.common.utils.DateTimeUtils;
import com.stately.common.utils.LunarUtils;
import com.stately.common.utils.StringUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import jxl.Workbook;
import jxl.write.DateTime;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

/**
 *
 * @author Edwin
 */
public class ExcelExporter implements Serializable
{

    private static final Logger logger = Logger.getLogger(ExcelExporter.class.getName());

    private File exportFile;
    private List<String> fieldsList = new LinkedList<>();
    private List<String> exclusingList = new LinkedList<>();

    private WritableWorkbook workbookXls = null;
    private SXSSFWorkbook workbookXlsx = null;

    private String fileFolder;
    public static String TEMP_DIR = System.getProperty("java.io.tmpdir");
    public static String USER_HOME = System.getProperty("user.home");

    private boolean formatHeader = true;
    private String[] sheetHeader;

    private final String DEFAULT_FILE_NAME = "DataExportFile";

    private boolean finalised;

    private Inclusiveness inclusiveness = Inclusiveness.EXCLUDE;

    private ExcelFormat excelFormat = ExcelFormat.XLSX;

    public static enum Inclusiveness
    {
        INCLUDE, EXCLUDE
    }

    public ExcelExporter withXls()
    {
        excelFormat = ExcelFormat.XLS;

        return this;
    }

    public ExcelExporter withXlsx()
    {
        excelFormat = ExcelFormat.XLSX;

        return this;
    }

    public ExcelExporter(String filePath)
    {
        init(filePath, DEFAULT_FILE_NAME, formatHeader);
    }

    public ExcelExporter()
    {
        init(TEMP_DIR, DEFAULT_FILE_NAME, formatHeader);
    }

    public ExcelExporter(String filePath, boolean formatHeader)
    {
        init(filePath, DEFAULT_FILE_NAME, formatHeader);
    }

    public ExcelExporter(ExcelFormat excelFormat, String directory, String fileName, boolean formatHeader)
    {
        this.excelFormat = excelFormat;
        init(directory, fileName, formatHeader);
    }

    public static ExcelExporter xlsx(String directory, String fileName, boolean formatHeader)
    {
        ExcelExporter exporter = new ExcelExporter(ExcelFormat.XLSX, directory, fileName, formatHeader);
        
        return exporter;
    }

    public ExcelExporter(String filePath, String fileName, boolean formatHeader)
    {
        init(filePath, fileName, formatHeader);
    }
    
    private void init(String fileFolder, String fileName, boolean formatHeader)
    {
        this.fileFolder = fileFolder;
        this.formatHeader = formatHeader;

        String extension = "." + excelFormat.getExtension();

        if (!fileName.endsWith(extension))
        {
            fileName += "" + extension;
        }

        try
        {

            File directory = new File(fileFolder);
            if (directory.exists() == false)
            {
                directory.mkdirs();
//                System.out.println("    directory created : " + directory);
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        exportFile = new File(fileFolder + File.separator + fileName);
        fileFolder = exportFile.getAbsolutePath();

        try
        {
            if (excelFormat.isXlx())
            {
                workbookXls = Workbook.createWorkbook(exportFile);
            } else if (excelFormat.isXlxs())
            {
                workbookXlsx = new SXSSFWorkbook(1000);
            }

//            System.out.println("Will be written to location: " + (filePath + fileName));
            System.out.println("Will be written to location: " + exportFile.getAbsolutePath());

//            System.out.println("written to : " + exportFile.getAbsolutePath());
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
            logger.log(Level.SEVERE, "could not create excel work book at location: {0}", exportFile);
        }
    }

    private void writeSheetHeader(WritableSheet sheet, List<Method> methods)
    {
        int col = 0;
        String fieldName;
        for (Method m : methods)
        {

            if (m.getReturnType() == boolean.class)
            {
                fieldName = LunarUtils.getVariableNameForDiplay(m.getName().substring(2));
            } else
            {
                fieldName = LunarUtils.getVariableNameForDiplay(m.getName().substring(3));
            }

            try
            {
                if (formatHeader)
                {
                    sheet.addCell(new Label(col++, 0, fieldName, Formating.defaultHeader()));
                } else
                {
                    sheet.addCell(new Label(col++, 0, fieldName, Formating.headerBold()));
                }
            } catch (WriteException writeException)
            {
                logger.log(Level.SEVERE, "Could not write header details");
            }
        }
    }

    private void writeSheetHeaderXls(WritableSheet sheet, List<Method> methods)
    {
        int col = 0;
        String fieldName;
        for (Method m : methods)
        {

            if (m.getReturnType() == boolean.class)
            {
                fieldName = LunarUtils.getVariableNameForDiplay(m.getName().substring(2));
            } else
            {
                fieldName = LunarUtils.getVariableNameForDiplay(m.getName().substring(3));
            }

            try
            {
                if (formatHeader)
                {
                    sheet.addCell(new Label(col++, 0, fieldName, Formating.defaultHeader()));
                } else
                {
                    sheet.addCell(new Label(col++, 0, fieldName, Formating.headerBold()));
                }
            } catch (WriteException writeException)
            {
                logger.log(Level.SEVERE, "Could not write header details");
            }
        }
    }

    public void writeSheetHeader(WritableSheet sheet)
    {
        int col = 0;
        for (String m : getSheetHeader())
        {
            try
            {
                if (formatHeader)
                {
                    sheet.addCell(new Label(col++, 0, m, Formating.defaultHeader()));
                } else
                {
                    sheet.addCell(new Label(col++, 0, m, Formating.headerBold()));
                }
            } catch (WriteException writeException)
            {
                logger.log(Level.SEVERE, "Could not write header details");
            }
        }
    }

    public InputStream getFileStream()
    {
        try
        {
            return FileUtils.openInputStream(exportFile);
        } catch (IOException e)
        {
            logger.log(Level.SEVERE, "error converting file to inputstream", e);
        }
        return null;
    }

    public void columnsInclude(String... inclustion)
    {
        fieldsList.addAll(Arrays.asList(inclustion));
    }

    public void exculudeColumns(String... exclusion)
    {
        exclusingList.addAll(Arrays.asList(exclusion));
    }

    public void columnsInclude(String inclustion)
    {
        fieldsList.add(inclustion);
    }

    private List<Method> filterMethods(List<Method> methods, Class t)
    {
        List<String> includeList = excludeOrIncludeList(t);

        if (includeList.isEmpty())
        {
            return methods;
        }

        List<Method> newMethods = new LinkedList<>();

        for (String field : includeList)
        {
            for (Method method : methods)
            {
                String methodName = method.getName().toLowerCase();
                if (methodName.startsWith("set"))
                {
                    continue;
                }

                if (method.getReturnType() == boolean.class)
                {
                    methodName = methodName.substring(2);
                } else
                {
                    methodName = methodName.substring(3);
                }

                if (field.toLowerCase().equals(methodName))
                {
                    newMethods.add(method);
                }
            }
        }
        return newMethods;
    }

    private List<String> excludeOrIncludeList(Class<?> type)
    {
        List<String> newFields = new LinkedList<>();
        List<Field> fields = ClassInfo.getInheritedFields(type);

        if (inclusiveness == Inclusiveness.INCLUDE && !fieldsList.isEmpty())
        {
            return fieldsList;
        }
        if (inclusiveness == null)
        {
            return Collections.EMPTY_LIST;
        }
        if (fieldsList.isEmpty())
        {
            for (Field field : fields)
            {
                newFields.add(field.getName());
            }

            return newFields;
        }

        for (Field field : fields)
        {
            for (String f : fieldsList)
            {
                if (f.equalsIgnoreCase(field.getName()))
                {
                    continue;
                }
                newFields.add(field.getName());
            }
        }
        System.out.println(newFields);
        return newFields;
    }

    public ExcelExporter addSheetData(List<?> data, Class<?> type, boolean include)
    {
        return addSheetData(data, type, type.getSimpleName(), include);
    }

    public ExcelExporter addSheetData(List<?> data, Class<?> type, String sheetName, boolean include)
    {
        try
        {
            if (StringUtil.isNullOrEmpty(sheetName))
            {
                sheetName = "Sheet-" + UUID.randomUUID().toString().replaceAll("-", "").substring(0, 5);
            }

            if (excelFormat == ExcelFormat.XLS)
            {
                addSheetDataXls(data, type, sheetName, include);
            } else if (excelFormat == ExcelFormat.XLSX)
            {
                addSheetDataXlsx(data, type, sheetName, include);
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        fieldsList = new LinkedList<>();
        return this;
    }
    public ExcelExporter addSheetData(List<Object[]> data, String sheetName, boolean include)
    {
        try
        {
            if (StringUtil.isNullOrEmpty(sheetName))
            {
                sheetName = "Sheet-" + UUID.randomUUID().toString().replaceAll("-", "").substring(0, 5);
            }

            if (excelFormat == ExcelFormat.XLS)
            {
//                addSheetDataXls(data, type, sheetName, include);
            } else if (excelFormat == ExcelFormat.XLSX)
            {
                addSheetDataXlsx(data, sheetName, include);
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        fieldsList = new LinkedList<>();
        return this;
    }

    public static List<Field> getFields(Class<?> type)
    {
        List<Field> fieldsList = new LinkedList<>();

        for (Class<?> c = type; c != null; c = c.getSuperclass())
        {
            fieldsList.addAll(Arrays.asList(c.getDeclaredFields()));
        }
        return fieldsList;
    }

    private ExcelExporter addSheetDataXlsx(List<?> data, Class<?> type, String sheetName, boolean include)
    {
        try
        {
            if (excelFormat != ExcelFormat.XLSX)
            {
                return this;
            }

            if (StringUtil.isNullOrEmpty(sheetName))
            {
                sheetName = "Sheet-" + UUID.randomUUID().toString().replaceAll("-", "").substring(0, 5);
            }

            if (include)
            {
                inclusiveness = Inclusiveness.INCLUDE;
            } else
            {
                inclusiveness = Inclusiveness.EXCLUDE;
            }

            List<Field> javaFieldsList = getFields(type);

            javaFieldsList = javaFieldsList.stream().filter(new Predicate<Field>()
            {
                @Override
                public boolean test(Field t)
                {
                    return !t.getName().startsWith("_");
                }
            }).collect(Collectors.toList());

//            System.out.println(javaFieldsList);
            for (Field field : javaFieldsList)
            {
                System.out.println(field.getName());
            }

//            List<Method> methods = filterMethods(ClassInfo.getInheritedMethods(type), type);
//            WritableSheet sheet = workbookXls.createSheet(sheetName, 1);
//            workbookXlsx.cre
//            XSSFSheet sheet = workbookXlsx.createSheet(sheetName);
            SXSSFSheet sheet = workbookXlsx.createSheet(sheetName);

            //create Headers
            Row header = sheet.createRow(0);
            int headerCol = 0;
            for (Field field : javaFieldsList)
            {
                field.setAccessible(true);
                String fieldName = field.getName();
                String colLabel = LunarUtils.getCamelCaseObjectName(fieldName);
                
                colLabel = LunarUtils.getVariableNameForDiplay(colLabel);
                if (field.isAnnotationPresent(ExcelColumn.class))
                {
                    ExcelColumn excelColumn = field.getAnnotation(ExcelColumn.class);
                    if (!StringUtil.isNullOrEmpty(excelColumn.label()))
                    {
                        colLabel = excelColumn.label();
                    }
                }

                short fontSize = 15;
                Cell cell = header.createCell(headerCol);
                cell.setCellValue(colLabel);
                CellStyle style = workbookXlsx.createCellStyle();
                Font font = workbookXlsx.createFont();
                font.setBold(true);
//                font.setFontHeight(fontSize);
                style.setFont(font);
                cell.setCellStyle(style);

                headerCol++;

            }

//            if (getSheetHeader() == null || getSheetHeader().length == 0)
//            {
//                writeSheetHeader(sheet, methods);
//            } else
//            {
//                writeSheetHeader(sheet);
//            }
            if (data.isEmpty())
            {
                logger.info("There's no data to populate this excel data");
                return this;
            }

//            int row = 1;
            System.out.println("data list ==== " + data.size());

            int numberOfRows = data.size();

            CellStyle dateCellStyle = workbookXlsx.createCellStyle();
            CreationHelper createHelper = workbookXlsx.getCreationHelper();
            dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy"));

            for (int i = 0; i < numberOfRows; i++)
            {
//                System.out.println("creating role ... " + i);
                Row row = sheet.createRow(i + 1);
//                System.out.println("row created --- " + row);

                Object value = data.get(i);
//                for (Object t : data)
//                {
                int col = -1;
                for (Field m : javaFieldsList)
                {
                    col++;
                    Cell cell = row.createCell(col);
                    try
                    {
//                        Object objVal = m.invoke(value);
                        Object objVal = m.get(value);

//                        System.out.println(" ----- " + objVal);
                        if (objVal == null)
                        {
                            continue;
                        }
                        if (m.getType() == String.class)
                        {
//                            cell.setCellType(CellType.STRING);
                            cell.setCellValue(objVal + "");
//                                sheet.addCell(new Label(col, row, ));
                        } else if (m.getType() == Integer.class)
                        {
//                            cell.setCellType(CellType.NUMERIC);
                            cell.setCellValue(ObjectValue.getIntegerValue(objVal));
//                                sheet.addCell(new jxl.write.Number(col, row, ObjectValue.getIntegerValue(objVal), Formating.NUMBER_FORMAT));
                        } else if (m.getType() == int.class)
                        {
//                            cell.setCellType(CellType.NUMERIC);
                            cell.setCellValue(ObjectValue.get_intValue(objVal));
//                                sheet.addCell(new jxl.write.Number(col, row, (int) objVal, Formating.NUMBER_FORMAT));
                        } else if (m.getType() == Double.class)
                        {
                            cell.setCellValue(ObjectValue.getDoubleValue(objVal));
                        } else if (m.getType() == BigDecimal.class)
                        {
                            cell.setCellValue(ObjectValue.getDoubleValue(objVal));
                        } else if (m.getType() == double.class)
                        {
//                            cell.setCellType(CellType.NUMERIC);
                            cell.setCellValue(ObjectValue.get_doubleValue(objVal));

//                                sheet.addCell(new jxl.write.Number(col, row, (double) objVal, Formating.DECIMAL_FORMAT));
                        } else if (m.getType() == Date.class)
                        {
//                                cell.setCellType(CellType.NUMERIC);
                            cell.setCellValue((Date) objVal);
                            cell.setCellStyle(dateCellStyle);
//                                sheet.addCell(new DateTime(col, row, (Date) objVal));
//                            System.out.println(cell.getDateCellValue()+ " @@@@@@@@@@@@");
                        } else if (m.getType() == LocalDate.class)
                        {
//                                cell.setCellType(CellType.NUMERIC);
                            Date date = DateTimeUtils.toDate((LocalDate) objVal);
                            cell.setCellValue(date);
                            cell.setCellStyle(dateCellStyle);
//                                sheet.addCell(new DateTime(col, row, (Date) objVal));
//                            System.out.println(cell.getDateCellValue()+ " @@@@@@@@@@@@");
                        } else
                        {
                            cell.setCellValue(objVal + "");
                        }
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                        logger.log(Level.SEVERE, "Error occured in filling an excel column {0} - " + e.getMessage(), m.getName());
                    }

                }
//                    row++;
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        System.out.println("finished writting to file memory --  " + exportFile);

        fieldsList = new LinkedList<>();
        return this;
    }

    private ExcelExporter addSheetDataXlsx(List<Object[]> data, String sheetName, boolean include)
    {
        try
        {
            if (excelFormat != ExcelFormat.XLSX)
            {
                return this;
            }

            if (StringUtil.isNullOrEmpty(sheetName))
            {
                sheetName = "Sheet-" + UUID.randomUUID().toString().replaceAll("-", "").substring(0, 5);
            }

            if (include)
            {
                inclusiveness = Inclusiveness.INCLUDE;
            } else
            {
                inclusiveness = Inclusiveness.EXCLUDE;
            }

            Object[] javaFieldsList = data.get(0);
            data.remove(0);
            

//            System.out.println(javaFieldsList);
            for (Object field : javaFieldsList)
            {
                System.out.println(field);
            }

//            List<Method> methods = filterMethods(ClassInfo.getInheritedMethods(type), type);
//            WritableSheet sheet = workbookXls.createSheet(sheetName, 1);
//            workbookXlsx.cre
//            XSSFSheet sheet = workbookXlsx.createSheet(sheetName);
            SXSSFSheet sheet = workbookXlsx.createSheet(sheetName);

            //create Headers
            Row header = sheet.createRow(0);
            int headerCol = 0;
            for (Object field : javaFieldsList)
            {
              
//                String colLabel = LunarUtils.getCamelCaseObjectName(fieldName);
//                if (field.isAnnotationPresent(ExcelColumn.class))
//                {
//                    ExcelColumn excelColumn = field.getAnnotation(ExcelColumn.class);
////                    System.out.println(excelColumn + " -- ");
//                    if (!StringUtil.isNullOrEmpty(excelColumn.label()))
//                    {
//                        colLabel = excelColumn.label();
//                    }
//                }

                Cell cell = header.createCell(headerCol);
                
                if(field != null)
                {
                    cell.setCellValue(field + "");
                }
                
                
                CellStyle style = workbookXlsx.createCellStyle();
                Font font = workbookXlsx.createFont();
                font.setBold(true);
                style.setFont(font);
                cell.setCellStyle(style);

                headerCol++;

            }

//            if (getSheetHeader() == null || getSheetHeader().length == 0)
//            {
//                writeSheetHeader(sheet, methods);
//            } else
//            {
//                writeSheetHeader(sheet);
//            }
            if (data.isEmpty())
            {
                logger.info("There's no data to populate this excel data");
                return this;
            }

//            int row = 1;
            System.out.println("data list ==== " + data.size());

            int numberOfRows = data.size();

            CellStyle dateCellStyle = workbookXlsx.createCellStyle();
            CreationHelper createHelper = workbookXlsx.getCreationHelper();
            dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy"));

            for (int i = 0; i < numberOfRows; i++)
            {
//                System.out.println("creating role ... " + i);
                Row row = sheet.createRow(i + 1);
//                System.out.println("row created --- " + row);

                Object[] value =data.get(i);
//                for (Object t : data)
//                {
                int col = -1;
                for (Object m : value)
                {
                    col++;
                    Cell cell = row.createCell(col);
                    try
                    {
//                        Object objVal = m.invoke(value);
                        Object objVal = m;

//                        System.out.println(" ----- " + objVal);
                        if (objVal == null)
                        {
                            continue;
                        }
                        if (m instanceof String)
                        {
//                            cell.setCellType(CellType.STRING);
                            cell.setCellValue(objVal + "");
//                                sheet.addCell(new Label(col, row, ));
                        } else if (m instanceof Integer)
                        {
//                            cell.setCellType(CellType.NUMERIC);
                            cell.setCellValue((Integer)objVal );
//                                sheet.addCell(new jxl.write.Number(col, row, ObjectValue.getIntegerValue(objVal), Formating.NUMBER_FORMAT));
                        } else if (m.getClass() == int.class)
                        {
//                            cell.setCellType(CellType.NUMERIC);
                            cell.setCellValue(objVal + "");
//                                sheet.addCell(new jxl.write.Number(col, row, (int) objVal, Formating.NUMBER_FORMAT));
                        } else if (m instanceof Double)
                        {
//                            cell.setCellType(CellType.NUMERIC);
                            cell.setCellValue((Double) objVal);
//                                sheet.addCell(new jxl.write.Number(col, row, new BigDecimal(String.valueOf(objVal)).doubleValue(), Formating.DECIMAL_FORMAT));
                        } else if (m.getClass() == BigDecimal.class)
                        {
//                            cell.setCellType(CellType.NUMERIC);
                            cell.setCellValue(objVal + "");
//                                sheet.addCell(new jxl.write.Number(col, row, ObjectValue.getDoubleValue(objVal), Formating.DECIMAL_FORMAT));
                        } else if (m.getClass() == double.class)
                        {
//                            cell.setCellType(CellType.NUMERIC);
                            cell.setCellValue((Double) objVal);

//                                sheet.addCell(new jxl.write.Number(col, row, (double) objVal, Formating.DECIMAL_FORMAT));
                        } else if (m.getClass() == Date.class)
                        {
//                                cell.setCellType(CellType.NUMERIC);
                            cell.setCellValue((Date) objVal);
                            cell.setCellStyle(dateCellStyle);
//                                sheet.addCell(new DateTime(col, row, (Date) objVal));
//                            System.out.println(cell.getDateCellValue()+ " @@@@@@@@@@@@");
                        } else if (m.getClass() == LocalDate.class)
                        {
//                                cell.setCellType(CellType.NUMERIC);
                            Date date = DateTimeUtils.toDate((LocalDate) objVal);
                            cell.setCellValue(date);
                            cell.setCellStyle(dateCellStyle);
//                                sheet.addCell(new DateTime(col, row, (Date) objVal));
//                            System.out.println(cell.getDateCellValue()+ " @@@@@@@@@@@@");
                        } else
                        {
                            cell.setCellValue(objVal + "");
                        }
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                        logger.log(Level.SEVERE, "Error occured in filling an excel column {0} - " + e.getMessage(), m);
                    }

                }
//                    row++;
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        System.out.println("finished writting to file memory --  " + exportFile);

        fieldsList = new LinkedList<>();
        return this;
    }

    private ExcelExporter addSheetDataXls(List<?> data, Class<?> type, String sheetName, boolean include)
    {
        try
        {
            if (excelFormat != ExcelFormat.XLS)
            {
                return this;
            }

            if (StringUtil.isNullOrEmpty(sheetName))
            {
                sheetName = "Sheet-" + UUID.randomUUID().toString().replaceAll("-", "").substring(0, 5);
            }

            if (include)
            {
                inclusiveness = Inclusiveness.INCLUDE;
            } else
            {
                inclusiveness = Inclusiveness.EXCLUDE;
            }

            List<Method> methods = filterMethods(ClassInfo.getInheritedMethods(type), type);
            WritableSheet sheet = workbookXls.createSheet(sheetName, workbookXls.getNumberOfSheets()+1);

            if (getSheetHeader() == null || getSheetHeader().length == 0)
            {
                writeSheetHeader(sheet, methods);
            } else
            {
                writeSheetHeader(sheet);
            }

            if (data.isEmpty())
            {
                logger.info("There's no data to populate this excel data");
                return this;
            }

            int row = 1;
            System.out.println("data list ==== " + data.size());

            for (Object t : data)
            {
                int col = -1;
                for (Method m : methods)
                {
                    col++;
                    try
                    {
                        Object objVal = m.invoke(t);

                        if (objVal == null)
                        {
                            continue;
                        }
                        if (m.getReturnType() == String.class)
                        {
                            sheet.addCell(new Label(col, row, (String) objVal + ""));
                        } else if (m.getReturnType() == Integer.class)
                        {
                            sheet.addCell(new jxl.write.Number(col, row, ObjectValue.getIntegerValue(objVal), Formating.NUMBER_FORMAT));
                        } else if (m.getReturnType() == int.class)
                        {
                            sheet.addCell(new jxl.write.Number(col, row, (int) objVal, Formating.NUMBER_FORMAT));
                        } else if (m.getReturnType() == Double.class)
                        {
                            sheet.addCell(new jxl.write.Number(col, row, new BigDecimal(String.valueOf(objVal)).doubleValue(), Formating.DECIMAL_FORMAT));
                        } else if (m.getReturnType() == BigDecimal.class)
                        {
                            sheet.addCell(new jxl.write.Number(col, row, ObjectValue.getDoubleValue(objVal), Formating.DECIMAL_FORMAT));
                        } else if (m.getReturnType() == double.class)
                        {
                            sheet.addCell(new jxl.write.Number(col, row, (double) objVal, Formating.DECIMAL_FORMAT));
                        } else if (m.getReturnType() == Date.class)
                        {
                            sheet.addCell(new DateTime(col, row, (Date) objVal));
                        } 
                        else if (m.getReturnType() == LocalDate.class)
                        {
                            try
                            {
                                sheet.addCell(new DateTime(col, row, DateTimeUtils.toDate((LocalDate)objVal)));
                            } catch (Exception e)
                            {
                            }
                        }
                        else
                        {
                            sheet.addCell(new Label(col, row, objVal.toString()));
                        }
                    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | WriteException e)
                    {
                        logger.log(Level.SEVERE, "Error occured in filling an excel column {0} - " + e.getMessage(), m.getName());
                    }

                }
                row++;
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        System.out.println("finished writting to sheet --- " + sheetName);

        fieldsList = new LinkedList<>();
        return this;
    }

    public ExcelExporter addSheetData(List<?> data, Class<?> type, String sheetName, Map<String, String> fieldLabelMap)
    {
        String header[] = new String[fieldLabelMap.size()];

        int counter = 0;
        for (Map.Entry<String, String> entry : fieldLabelMap.entrySet())
        {
            String key = entry.getKey();
            String value = entry.getValue();

            header[counter] = value;
            fieldsList.add(key);

            counter++;
        }

//          String fileName = "InvestmentReport";
//        String sheetHeader[]
//                = {
//                    "Name of Scheme", "Date of Investment", "Name of Issuer", "Asset Class", "Asset Tenor",
//                    "Amount Invested", "Expected Return", "Commulative Amount", "Interest Earned"
//                };
//        ExcelExporter excelExporter = new ExcelExporter(ExcelExporter.TEMP_DIR + fileName, true);
        setSheetHeader(header);
//        excelExporter.columnsInclude("fundName", "transDate", "issuer", "incomeClass", "tenor", "cost", "coupon", "maturityValue", "interest");
//        return addSheetData(data, type, true);
        return addSheetData(data, type, sheetName,true);

    }

    public void finalise()
    {

        try
        {
            if (excelFormat.isXlx())
            {
                workbookXls.write();
                workbookXls.close();
                finalised = true;
            } else if (excelFormat.isXlxs())
            {
                try (FileOutputStream outputStream = new FileOutputStream(exportFile.getAbsolutePath()))
                {
                    workbookXlsx.write(outputStream);
                }
//                workbookXlsx.close();
                workbookXlsx.dispose();
                finalised = true;
            }

        } catch (IOException | WriteException e)
        {
            e.printStackTrace();
            logger.log(Level.SEVERE, "Error closing workbook");
        }
    }

    public File getExportFile()
    {
        return exportFile;
    }

    public String[] getSheetHeader()
    {
        return sheetHeader;
    }

    public void setSheetHeader(String[] sheetHeader)
    {
        this.sheetHeader = sheetHeader;
    }

    public boolean isFinalised()
    {
        return finalised;
    }
    
    public void deleteFile()
    {
        try
        {
            exportFile.delete();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void main(String[] args)
    {
        DateRange dateRange1 = new DateRange(new Date(), new Date());
        DateRange dateRange2 = new DateRange(new Date(), new Date());
        DateRange dateRange3 = new DateRange(new Date(), new Date());

        List<DateRange> dateRangesList = Arrays.asList(dateRange1, dateRange2, dateRange3);

        Map<String, String> mapsList = new LinkedHashMap<>();
        mapsList.put("fromDate", "Fund Name");
        mapsList.put("toDate", "Transaction Date");
        
        List<Object[]> dataList = new LinkedList<>();
        for (int i = 0; i < 4; i++)
        {
            Object[] item = new Object[3];
            item[0] = "Hello";
            item[1] = "Hello";
            item[2] = "Hello";
            
            dataList.add(item);
            
        }

//        ExcelExporter excelExporter = new ExcelExporter(ExcelExporter.TEMP_DIR, "file.xls", true);
        ExcelExporter excelExporter = new ExcelExporter(ExcelFormat.XLSX, ExcelExporter.TEMP_DIR, "file", true);
//        ExcelExporter excelExporter = new ExcelExporter(ExcelFormat.XLS, ExcelExporter.TEMP_DIR, "file", true);
        excelExporter.addSheetData(dateRangesList, DateRange.class, "file", mapsList);
        excelExporter.addSheetData(dataList, "file2", true);
        excelExporter.finalise();
    }

}
