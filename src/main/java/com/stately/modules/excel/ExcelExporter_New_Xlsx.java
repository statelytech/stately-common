/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.modules.excel;

import com.google.common.base.Strings;
import com.stately.common.formating.ObjectValue;
import com.stately.common.model.DateRange;
import com.stately.common.reflect.ClassInfo;
import com.stately.common.utils.LunarUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Edwin
 */
public class ExcelExporter_New_Xlsx extends ExcelExporter_New_Interface
{
    private static final Logger logger = Logger.getLogger(ExcelExporter_New_Xlsx.class.getName());
    
    private File exportFile;
    private List<String> fieldsList = new LinkedList<>();
    private List<String> exclusingList = new LinkedList<>();
    
    private XSSFWorkbook workbookXlsx = null;
    
    private  String filePath;
    public static String TEMP_DIR = System.getProperty("java.io.tmpdir");
    public static String USER_HOME = System.getProperty("user.home");
    
    private boolean formatHeader = true;
    private String[] sheetHeader;
    
    private final String DEFAULT_FILE_NAME = "DataExportFile";
    
    private boolean finalised;
    
    private Inclusiveness inclusiveness = Inclusiveness.EXCLUDE;
    
    private ExcelFormat excelFormat = ExcelFormat.XLSX ;
    
    
    public static enum  Inclusiveness{
        INCLUDE, EXCLUDE
    }
    
    
    
    public ExcelExporter_New_Xlsx(String filePath)
    {
        init(filePath, DEFAULT_FILE_NAME, formatHeader);
//        this.filePath = filePath;
//        exportFile = new File(filePath + "data_file.xls");
//        
//        try 
//        {
//            workbook = Workbook.createWorkbook(exportFile);
//        } catch (IOException e)
//        {
//            logger.log(Level.INFO, "error in initialising excel export .. {0}", e.getMessage());
//        }
    }
    
    
    
    public ExcelExporter_New_Xlsx(String filePath, boolean formatHeader)
    {
        init(filePath, DEFAULT_FILE_NAME, formatHeader);
    }
    
    public ExcelExporter_New_Xlsx(String filePath, String fileName, boolean formatHeader)
    {
        init(filePath, fileName, formatHeader);
    }
    
    
    private void init(String fileFolder, String fileName, boolean formatHeader)
    {
        this.filePath = fileFolder;
        this.formatHeader = formatHeader;
        
        String extension = "."+excelFormat.getExtension();
        
        if(!fileName.endsWith(extension))
        {
            fileName += ""+extension;
        }
        
        
        try
        {
            File directory = new File(fileFolder);
            if(directory.exists() == false)
            {
                directory.mkdirs();
//                System.out.println("    directory created : " + directory);
            }
            
        } catch (Exception e)
        {
            e.printStackTrace();
        }
                
        exportFile = new File(fileFolder + File.separator + fileName);
        
        try 
        {  
            
            workbookXlsx = new XSSFWorkbook();
            
//            System.out.println("Will be written to location: " + (filePath + fileName));
            System.out.println("Will be written to location: " + exportFile.getAbsolutePath());
          
            System.out.println("written to : " + exportFile.getAbsolutePath());
        } 
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            logger.log(Level.SEVERE, "could not create excel work book at location: {0}", exportFile);
        }
    }
    
    private void writeSheetHeader(Sheet sheet, List<Method> methods)
    {
        int col = 0;
        String fieldName;
        
        Row headerRow = sheet.createRow(0);
        
        for (Method m : methods) {
            
            if(m.getReturnType() == boolean.class)
            {
                fieldName = LunarUtils.getVariableNameForDiplay(m.getName().substring(2));
            }
            else
            {
                fieldName = LunarUtils.getVariableNameForDiplay(m.getName().substring(3));
            }
            
            try
            {
                Cell cell1 = headerRow.createCell(col++);
                cell1.setCellValue(fieldName);
                
//                if(formatHeader)
//                {
//                    sheet.addCell(new Label(col++, 0, fieldName, Formating.defaultHeader()));
//                }
//                else
//                {
//                    sheet.addCell(new Label(col++, 0, fieldName, Formating.headerBold()));
//                }
            } 
            catch (Exception writeException)
            {
                logger.log(Level.SEVERE, "Could not write header details");
            }
        }
    }
    
    public void writeSheetHeader(Sheet sheet)
    {
        int col = 0;
        Row headerRow = sheet.createRow(0);
        for (String m : getSheetHeader())
        {
            try
            {
                Cell cell1 = headerRow.createCell(col++);
                cell1.setCellValue(m);
//                if(formatHeader)
//                {
//                    sheet.addCell(new Label(col++, 0, m, Formating.defaultHeader()));
//                }
//                else
//                {
//                    sheet.addCell(new Label(col++, 0, m, Formating.headerBold()));
//                }
            } 
            catch (Exception writeException)
            {
                logger.log(Level.SEVERE, "Could not write header details");
            }
        }
    }

    public InputStream getFileStream()
    {
        try 
        {
            return FileUtils.openInputStream(exportFile);
        } catch (IOException e)
        {
            logger.log(Level.SEVERE, "error converting file to inputstream", e);
        }
        return null;
    }
    
    public void add(String... inclustion)
    {
        fieldsList.addAll(Arrays.asList(inclustion));
    }
    
    public void addExclusion(String... exclusion)
    {
        exclusingList.addAll(Arrays.asList(exclusion));
    }
    
    public void add(String inclustion)
    {
        fieldsList.add(inclustion);
    }
    
    private List<Method> filterMethods(List<Method> methods, Class t)
    {
        List<String> includeList = excludeOrIncludeList(t);
        
        if(includeList.isEmpty())
        {
            return methods;
        }
        
        List<Method> newMethods = new LinkedList<>();
        
        for(String field : includeList)
        {
            for (Method method : methods)
            {
                String methodName = method.getName().toLowerCase();
                if(methodName.startsWith("set"))
                {
                    continue;
                }
                
                if(method.getReturnType() == boolean.class)
                {
                    methodName = methodName.substring(2);
                }
                else
                {
                    methodName = methodName.substring(3);
                }
                
                if(field.toLowerCase().equals(methodName))
                {
                    newMethods.add(method);
                }
            }
        }
        return newMethods;
    }
    
    
    
    
    private List<String> excludeOrIncludeList(Class<?> type)
    {
        List<String> newFields = new LinkedList<>();
        List<Field> fields = ClassInfo.getInheritedFields(type);
        
        if(inclusiveness == Inclusiveness.INCLUDE && !fieldsList.isEmpty())
        {
            return fieldsList;
        }
        if(inclusiveness == null)
        {
            return Collections.EMPTY_LIST;
        }
        if(fieldsList.isEmpty())
        {
            for (Field field : fields) 
            {
                newFields.add(field.getName());
            }
            
            return newFields;
        }
        
        for (Field field : fields) 
        {
            for(String f : fieldsList)
            {
                if(f.equalsIgnoreCase(field.getName()))
                {
                    continue;
                }
                newFields.add(field.getName());
            }
        }
        System.out.println(newFields);
        return newFields;
    }
    
    public ExcelExporter_New_Xlsx addSheetData(List<?> data, Class<?> type, boolean include)
    {
        return addSheetData(data, type, type.getSimpleName(), include);
    }
    
    public ExcelExporter_New_Xlsx addSheetData(List<?> data, Class<?> type, String sheetName, boolean include)
    {
        if(Strings.isNullOrEmpty(sheetName))
        {
            sheetName = "Sheet-" +UUID.randomUUID().toString().replaceAll("-", "").substring(0, 5);
        }
        
        if(include)
        {
            inclusiveness = Inclusiveness.INCLUDE;
        }
        else
        {
            inclusiveness = Inclusiveness.EXCLUDE;
        }
        
        List<Method> methods = filterMethods(ClassInfo.getInheritedMethods(type), type);
//        WritableSheet sheet = workbookXls.createSheet(sheetName, 1);
        Sheet sheet = workbookXlsx.createSheet();
        
        if(getSheetHeader() == null || getSheetHeader().length == 0)
        {
            writeSheetHeader(sheet, methods);
        }
        else
        {
            writeSheetHeader(sheet);
        }
        
        if(data.isEmpty())
        {
            logger.info("There's no data to populate this excel data");
            return this;
        }
        
//        int row = 1;
        System.out.println("data list ==== " + data.size());
    
        int rowCounter = 1;
            
            for (Object t : data)
            {
                Row row  = sheet.createRow(rowCounter++);
                int col = -1;
                for (Method m : methods)
                {
                    col++;
                    try
                    {
                        Object objVal = m.invoke(t);

                        
                        Cell cell = row.createCell(col);
                        
                        if (objVal == null)
                        {
                            continue;
                        }
                        if (m.getReturnType() == String.class)
                        {
                            cell.setCellValue((String) objVal);
                        } else if (m.getReturnType() == Integer.class)
                        {
                            cell.setCellValue(ObjectValue.getIntegerValue(objVal));
                            cell.setCellType(CellType.NUMERIC);
//                            sheet.addCell(new jxl.write.Number(col, row, , Formating.NUMBER_FORMAT));
                        } else if (m.getReturnType() == int.class)
                        {
                            cell.setCellValue(ObjectValue.getIntegerValue(objVal));
                            cell.setCellType(CellType.NUMERIC);
                        } else if (m.getReturnType() == Double.class)
                        {
                             cell.setCellValue(ObjectValue.getDoubleValue(objVal));
                            cell.setCellType(CellType.NUMERIC);
                        } else if (m.getReturnType() == BigDecimal.class)
                        {
                            cell.setCellValue(ObjectValue.getDoubleValue(objVal));
                            cell.setCellType(CellType.NUMERIC);
                        } else if (m.getReturnType() == double.class)
                        {
                            cell.setCellValue(ObjectValue.getDoubleValue(objVal));
                            cell.setCellType(CellType.NUMERIC);
                        } else if (m.getReturnType() == Date.class)
                        {
                            cell.setCellValue((Date)objVal);
                        } else
                        {
                           cell.setCellValue((String)objVal);
                        }
                    } catch (Exception e)
                    {
                        logger.log(Level.SEVERE, "Error occured in filling an excel column {0} - " + e.getMessage(), m.getName());
                    }

                }
//                row++;
            }
        
        
        fieldsList = new LinkedList<>();
        return this;
    }
        
    
    public ExcelExporter_New_Xlsx addSheetData(List<?> data, Class<?> type, String sheetName, Map<String, String> fieldLabelMap)
    {
        String header[] = new String[fieldLabelMap.size()];
        
        int counter = 0;
        for (Map.Entry<String, String> entry : fieldLabelMap.entrySet())
        {
            String key = entry.getKey();
            String value = entry.getValue();
            
            header[counter] = value;
            fieldsList.add(key);
            
            counter++;
        }
        
//          String fileName = "InvestmentReport";
//        String sheetHeader[]
//                = {
//                    "Name of Scheme", "Date of Investment", "Name of Issuer", "Asset Class", "Asset Tenor",
//                    "Amount Invested", "Expected Return", "Commulative Amount", "Interest Earned"
//                };
//        ExcelExporter excelExporter = new ExcelExporter(ExcelExporter.TEMP_DIR + fileName, true);
        setSheetHeader(header);
//        excelExporter.add("fundName", "transDate", "issuer", "incomeClass", "tenor", "cost", "coupon", "maturityValue", "interest");
        return addSheetData(data, type, true);
       
        
        
    }
        
    
    
    
    public void finalise()
    {
        
        try 
        {
            
                try (FileOutputStream outputStream = new FileOutputStream(exportFile.getAbsolutePath())) {
                    workbookXlsx.write(outputStream);
                }
                workbookXlsx.close();
                finalised = true;
          
            
        } 
        catch (Exception e) 
        {
            logger.log(Level.SEVERE, "Error closing workbook");
        }
    }

    public File getExportFile() {
        return exportFile;
    }

    public String[] getSheetHeader() {
        return sheetHeader;
    }

    public void setSheetHeader(String[] sheetHeader) {
        this.sheetHeader = sheetHeader;
    }

    public boolean isFinalised()
    {
        return finalised;
    }
    
    
    
    public static void main(String[] args)
    {
        DateRange dateRange1 = new DateRange(new Date(), new Date());
        DateRange dateRange2 = new DateRange(new Date(), new Date());
        DateRange dateRange3 = new DateRange(new Date(), new Date());
        
        List<DateRange> dateRangesList = Arrays.asList(dateRange1,dateRange2, dateRange3);
        
        Map<String,String> mapsList = new LinkedHashMap<>();
        mapsList.put("fromDate", "Fund Name");
        mapsList.put("toDate", "Transaction Date");
        
         ExcelExporter_New_Xlsx excelExporter = new ExcelExporter_New_Xlsx(ExcelExporter_New_Xlsx.TEMP_DIR + "file.xls", true);
        excelExporter.addSheetData(dateRangesList,  DateRange.class, "file.xls", mapsList);
        excelExporter.finalise();
    }
    
    
}
