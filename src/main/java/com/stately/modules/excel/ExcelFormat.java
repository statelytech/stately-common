/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.modules.excel;

/**
 *
 * @author Edwin
 */
public enum ExcelFormat
{
    XLS("xls"),XLSX("xlsx");
    String extension;

    private ExcelFormat(String extension)
    {
        this.extension = extension;
    }

    public String getExtension()
    {
        return extension;
    }
    
    
    public boolean isXlxs()
    {
        return this == XLSX;
    }
    
    
    public boolean isXlx()
    {
        return this == XLS;
    }
    
    
    
}
