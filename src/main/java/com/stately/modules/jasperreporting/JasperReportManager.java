/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.modules.jasperreporting;

import com.stately.common.utils.StringUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
//import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.export.SimpleDocxReportConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Edwin
 */
public class JasperReportManager implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(JasperReportManager.class.getName());

    private final Map<String, Object> defaultParamenters = new HashMap<>();
    private final Map<String, Object> reportParamenters = new HashMap<>();
    
    private ReportOutputFileType reportOutputFileType = ReportOutputFileType.PDF;
    private ReportDesignFileType reportFileType;
    private ReportOutputEnvironment reportOutputEnvironment;
    private JasperPrint jasperPrint;
    private String jasperFile;
    private Collection reportDataList;
    
    String msg = "";
    private String reportTitle;
    
    public static final String TEMP_DIR = System.getProperty("java.io.tmpdir");
    public static final String REPORT_TITLE = "reportTitle";
    
    private String writeFolder = TEMP_DIR;
    
    private boolean writeReportToFile = false;

    public void createTemWriteDir(String folderName)
    {
        writeFolder = new File(TEMP_DIR, folderName).getAbsolutePath();
    }

    public void addMap(Map reportParameters) {
        for (Object key : reportParameters.keySet()) {
            addParam(key.toString(), reportParameters.get(key));
        }
    }

    public void showReport(ReportData reportCreationData) 
    {
        reportParamenters.putAll(reportCreationData.getMap());
        showReport(reportCreationData.getListData(), reportCreationData.getReportFile());
    }

    public void showReport(Collection reportData, InputStream inputStream) {
        this.reportDataList = reportData;
        jasperPrint = createJasperPrint(inputStream,reportDataList);
        produceFinalReport();
    }

    public boolean isVariableSet() {

        return true;
    }

    public void showReport(Collection reportData, String jasperFile) 
    {
        this.reportDataList = reportData;
        this.jasperFile = jasperFile;

        jasperPrint = createJasperPrint(null,reportDataList);

        produceFinalReport();
    }

    public void showReport(Object reportData, String jasperFile) 
    {
        List dataList = new ArrayList(1);
        dataList.add(reportData);
        showReport(dataList, jasperFile);
    }

    
    public void showReport(Object reportData, InputStream reportInputStrem) {
        List dataList = new ArrayList(1);
        dataList.add(reportData);
        showReport(dataList, reportInputStrem);
    }

    public void showHtmlReport(Collection reportData, String jasperFile) {
        this.reportDataList = reportData;
        this.jasperFile = jasperFile;

        createJasperPrint(null,reportDataList);

        produceFinalReport();
    }

    private void runWebEnviromentReport() {

        HttpServletResponse response = getServeltResponse();

        

//         if (pdf == null) {
//            res.setContentType("text/plain");
//            res.getWriter().write("No document available.");
//        } else {
//            res.setContentType("application/pdf");
//            res.setHeader("Content-Disposition", "inline");
//            res.setHeader("Accept-Ranges", "bytes");
//        HttpServletRequest request = getServeltRequest();

//        response.setContentType(reportOutput.getContentType());
        if (reportOutputFileType == null) {
            LOGGER.info("Report outfile type is not set, PDF will be assumed");
            reportOutputFileType = ReportOutputFileType.PDF;
        }

        try {
            
             String reportName = "ReportFile";
            
            switch (reportOutputFileType) {
                case PDF:
                    
                    response.setContentType("application/pdf");
                    
                    JRPdfExporter pdfExporter = new JRPdfExporter();

                    //Deprecated way
//                    pdfExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
//                    pdfExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());

                    //New way
                    pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                    pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                    SimplePdfExporterConfiguration pdfConfiguration = new SimplePdfExporterConfiguration();
                    pdfExporter.setConfiguration(pdfConfiguration);
                    
                    
                    
                    pdfExporter.exportReport();

                    break;
                case XHTML:                    
//                    Jrht exporter = new JRHtmlExporter();
//                    
//                    exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//                    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(destFile));
//                    SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
//                    configuration.setOnePagePerSheet(true);
//                    configuration.setDetectCellType(true);
//                    configuration.setCollapseRowSpan(false);
//                    exporter.setConfiguration(configuration);
//
//                    exporter.exportReport();
                    
//                    JRHtmlExporter htmlExporter = new JRHtmlExporter();
//                    request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);
//                    htmlExporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, "image?image=");
//
//                    htmlExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
//                    htmlExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, response.getOutputStream());
//
//                    htmlExporter.exportReport();

                    break;
                case EXCEL:
                    
//                    response.setContentType("application/vnd.ms-excel");
                    response.setContentType("application/octet-stream");
                    
                    response.setHeader("Cache-Control", "max-age=30");
                    response.setHeader("Content-Disposition", "attachment;filename="+reportName+".xls");    
                    response.setHeader("Content-disposition", "inline; filename=\"" + reportName + "_"  + ".xls\"");
                    
                    
                    JRXlsExporter exporter = new JRXlsExporter();
                    
//                    File outputFile = new File("output.xlsx");
//                     OutputStream fileOutputStream = new FileOutputStream(outputFile);
                    
                    exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
                    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
                    SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
//                    configuration.setOnePagePerSheet(true);
                    configuration.setDetectCellType(true);
                    configuration.setCollapseRowSpan(false);
                    configuration.setIgnoreGraphics(false);
                    exporter.setConfiguration(configuration);

                    exporter.exportReport();

                    break;
                case WORD:

                   
                    
//                    response.setContentType("application/vnd.ms-excel");
                    response.setContentType(ReportOutputFileType.WORD.getContentType());
//                    response.setContentType("application/octet-stream");
                    
                    
                    response.setHeader("Cache-Control", "max-age=30");
                    response.setHeader("Content-Disposition", "attachment;filename="+reportName+".docx");    
                    response.setHeader("Content-disposition", "inline; filename=\"" + reportName + "_"  + ".docx\"");
                    
                               JRDocxExporter export = new JRDocxExporter();
                    export.setExporterInput(new SimpleExporterInput(jasperPrint));
                    export.setExporterOutput(new SimpleOutputStreamExporterOutput(new File(reportName + ".docx")));

                    SimpleDocxReportConfiguration config = new SimpleDocxReportConfiguration();
                    config.setFlexibleRowHeight(true); //Set desired configuration

                    export.setConfiguration(config);
                    export.exportReport();
                    
//                    File outputFile = new File("output.xlsx");
//                     OutputStream fileOutputStream = new FileOutputStream(outputFile);
                    
//                    export.setExporterInput(new SimpleExporterInput(jasperPrint));
//                    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
//                    SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
//                    configuration.setOnePagePerSheet(true);
//                    configuration.setDetectCellType(true);
//                    configuration.setCollapseRowSpan(false);
//                    configuration.setIgnoreGraphics(false);
//                    exporter.setConfiguration(configuration);

//                    exporter.exportReport();

                    break;
            }

        } catch (Exception e) {
            Logger.getLogger(JasperReportManager.class.getName()).log(Level.SEVERE, e.toString());
        }

        try {
            FacesContext.getCurrentInstance().responseComplete();
        } catch (Exception e) {
            Logger.getLogger(JasperReportManager.class.getName()).log(Level.SEVERE, e.getMessage(), e);
        }

    }

    private void desktopEnviroment() {

        JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);

//         if(reportTitle == null)
//         {
//             reportTitle = (String) reportParamenters.get(REPORT_TITLE);
//         }
        try {
            reportTitle = (String) reportParamenters.get(REPORT_TITLE);
        } catch (Exception e) {
        }

        jasperViewer.setTitle(reportTitle);
        jasperViewer.setVisible(true);

    }


    public String writeToFile(Collection reportData, InputStream inputStream, String reportDirectory, String fileName, String password) 
    {
        String logMessage = "";
        
        reportDirectory = reportDirectory.trim();

        if (fileName == null || reportDirectory == null) {
            logMessage = "Please report directory and file name can not be NULL \n"
                    + "REPORT_DIRECTORY = " + reportDirectory + "\n"
                    + "FILE_NAME = " + fileName;

            Logger.getLogger(JasperReportManager.class.getName()).log(Level.INFO, logMessage);

            return null;

        }

        fileName = checkFileName(fileName);

        try {
            new File(reportDirectory.trim()).mkdirs();
        } catch (Exception e) {
            logMessage = "Unable to create or find Report Output directory (" + reportDirectory + ")";
            Logger.getLogger(JasperReportManager.class.getName()).log(Level.SEVERE, logMessage + "\n" + e.getMessage(), e);
            return null;
        }

        File file = new File(reportDirectory, fileName);
        
        JasperPrint jprint = createJasperPrint(inputStream,reportData);
        if(jprint != null && !StringUtil.isNullOrEmpty(password))
        {
            jprint.setProperty("net.sf.jasperreports.export.pdf.encrypted", "True");
            jprint.setProperty("net.sf.jasperreports.export.pdf.128.bit.key", "True");
            jprint.setProperty("net.sf.jasperreports.export.pdf.permissions.allowed", "PRINTING");
            jprint.setProperty("net.sf.jasperreports.export.pdf.user.password", password);
            jprint.setProperty("net.sf.jasperreports.export.pdf.owner.password",password);
        }

        msg = "Report will be written to " + file.getAbsolutePath();
        Logger.getLogger(JasperReportManager.class.getName()).log(Level.INFO, logMessage);
        
        switch (reportOutputFileType) 
        {
            case PDF:
                try 
                {
                    
                    JasperExportManager.exportReportToPdfFile(jprint, file.getAbsolutePath());

                    LOGGER.info("Report exported to pdf completed - " + file.getAbsolutePath());

//                    setGeneratedFile(file);

                    return file.toString();

                } catch (Exception ex) {
                    ex.printStackTrace();
                    Logger.getLogger(JasperReportManager.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                }
                break;
                case EXCEL:
                try 
                {
                    
                    
                           SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
//                    configuration.setOnePagePerSheet(true);
                    configuration.setDetectCellType(true);
                    configuration.setCollapseRowSpan(false);
                    configuration.setIgnoreGraphics(false);
                    
                    
                    
                    JRXlsExporter exporter = new JRXlsExporter();
                    
                    File outputFile = new File(file.getAbsolutePath());
                    System.out.println("excel write to file ..... >>>>  " + outputFile);
                     FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
                    
                    exporter.setExporterInput(new SimpleExporterInput(jprint));
                    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(fileOutputStream));
                    exporter.setConfiguration(configuration);

                    exporter.exportReport();
                    
                     fileOutputStream.flush();
        fileOutputStream.close();
                    
                    
                    
                    
//                    JasperExportManager.exp(jprint, file.getAbsolutePath());

                    LOGGER.info("Report exported to html completed - " + file.getAbsolutePath());

//                    setGeneratedFile(file);

                    return file.toString();

                } catch (Exception ex) {
                    ex.printStackTrace();
                    Logger.getLogger(JasperReportManager.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
                }
        }

        return null;
    }

    public String writeToFile(ReportData reportData, String reportDirectory, String fileName) 
    {
        InputStream inputStream = JasperReportManager.class.getResourceAsStream(reportData.getReportFile());
        
        if(inputStream == null)
        {
            System.out.println("-- returning ... input stream is null for generating report with data - " + reportData);
            return null;
        }
        
        addMap(reportData.getMap());

        jasperFile = reportData.getReportFile();
        
        return writeToFile(reportData.getListData(), inputStream, reportDirectory, fileName,reportData.getPdfPassword());
    }

    public String writeToFile(ReportData reportData, String fileName) 
    {
        return writeToFile(reportData, writeFolder, fileName);
    }


    public void addParam(String paramKey, Object paramValue) {
        reportParamenters.put(paramKey, paramValue);
    }

    public static HttpServletResponse getServeltResponse() {
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().
                getExternalContext().getResponse();

        return response;
    }

    public static HttpServletRequest getServeltRequest() {
        HttpServletRequest response = (HttpServletRequest) FacesContext.getCurrentInstance().
                getExternalContext().getRequest();

        return response;
    }

    public String checkFileName(String fileName) {
        String oldFilename = fileName;

        char[] chrs = new char[]{'\'', '/', ':', '*', '?', '"', '<', '>', '|'};
        if (!StringUtils.containsAny(fileName, chrs)) {
            return fileName;
        }

        for (int i = 0; i < chrs.length; i++) {
            char c = chrs[i];
            fileName = StringUtils.remove(fileName, c);
        }

        msg = "Report Output File Name (" + oldFilename + ") contains escape or invalid charaters. "
                + "Will be replaced with (-) to " + fileName;
        Logger.getLogger(JasperReportManager.class.getName()).log(Level.INFO, msg);
        return fileName;
    }

    private static void forceDownload() {
        HttpServletResponse hsr = getServeltResponse();
        hsr.setHeader("Content-type", "application/force-download");
        hsr.setHeader("Content-Transfer-Encoding", "Binary");
        hsr.setHeader("Content-length", null);
//        hsr.setContentType("application/force-download");
//        hsr.setCharacterEncoding("Binary");

    }

    private void produceFinalReport() 
    {
        if (jasperPrint == null) {
            msg = "Could not create Jasper Print so Report Process will be abborted";
            Logger.getLogger(JasperReportManager.class.getName()).log(Level.SEVERE, msg);

            return;
        }

        
        
        switch (reportOutputEnvironment) 
        {
            case WEB_APPLICATION:
                runWebEnviromentReport();
                break;
            case DESKTOP_APPLICATION:
                desktopEnviroment();
                break;
        }
    }

    public JasperPrint createJasperPrint(InputStream rptIns, Collection reportDataList) 
    {
        JasperPrint jPrint = null;
        JRBeanCollectionDataSource jrCollectionDataSource = null;
        try {
            jrCollectionDataSource = new JRBeanCollectionDataSource(reportDataList,false);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }

        if (reportFileType == null) {
            throw new IllegalArgumentException("Please specify report file type : " + reportFileType);
        }

        if (reportFileType == ReportDesignFileType.INPUTSTREAM) 
        {
            InputStream inputStream = null;

            if (rptIns != null) 
            {
                inputStream = rptIns;
            } 
            else if (!StringUtil.isNullOrEmpty(jasperFile))
            {
                try 
                {
//                    System.out.println("about to load input stream with " + jasperFile);

                    if (!jasperFile.endsWith(".jasper")) {
                        jasperFile = jasperFile + ".jasper";
                    }

                    inputStream = JasperReportManager.class.getResourceAsStream(jasperFile);
//                    System.out.println(inputStream + " result of searchin.... " + jasperFile);
                } catch (Exception e) {
                    msg = "Unable to load Input Stream for " + jasperFile;
                    Logger.getLogger(JasperReportManager.class.getName()).log(Level.SEVERE, msg + " \n " + e.toString(), e);

                    jPrint = null;

                    return jPrint;
                }
            }

            try {
                msg = "Creating JasperPrint with inputstream: " + inputStream
                        + " and bean collection datasource " + jrCollectionDataSource;

                Logger.getLogger(JasperReportManager.class.getName()).log(Level.INFO, msg);

                if (inputStream == null) {
                    return jPrint;
                }

//                System.out.println("going to create jasper print from : " + inputStream);
                jPrint = JasperFillManager.fillReport(inputStream, reportParamenters, jrCollectionDataSource);

                System.out.println("jasperPrint created = " + jPrint);

            } catch (Exception e) {
                Logger.getLogger(JasperReportManager.class.getName()).log(Level.SEVERE,
                        "Error Creating JasperPrint for " + jasperFile + "\n" + e.toString(), e);

            }
        } else if (reportFileType == ReportDesignFileType.STRING_FILE) {
        }
        
        return jPrint;
    }

    public void addToDefaultParameters(String paramKey, Object paramValue) {
        defaultParamenters.put(paramKey, paramValue);
        reportParamenters.put(paramKey, paramValue);
    }

    public void resetReportParametersToDefault() {
        reportParamenters.clear();
        reportParamenters.putAll(defaultParamenters);
    }

    public Map<String, Object> getReportParamenters() {
        return reportParamenters;
    }

    public ReportOutputEnvironment getReportEnvironment() {
        return reportOutputEnvironment;
    }

    public void setReportEnvironment(ReportOutputEnvironment reportEnvironment) {
        this.reportOutputEnvironment = reportEnvironment;
    }

    public ReportDesignFileType getReportFileType() {
        return reportFileType;
    }

    public void setReportFileType(ReportDesignFileType reportFileType) {
        this.reportFileType = reportFileType;
    }

    public ReportOutputFileType getReportOutput() {
        return reportOutputFileType;
    }

    public void setReportOutput(ReportOutputFileType reportOutput) {
        this.reportOutputFileType = reportOutput;
    }

    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }

//    <?php
//   $dir="resource/";
//   $BASE="../";
//
//   $filetitle= "DOWNLOADING  file ";
//   $file=$_GET["file"];
//
//    header("Content-type: application/force-download");
//    header("Content-Transfer-Encoding: Binary");
//    header("Content-length: ".filesize($file));
//    header("Content-disposition: attachment; filename=\"".basename($file)."\"");
//    readfile("$file");
// ?>
    private void create(String files) {
        File pa = new File(files);

        try {
            pa.mkdirs();
        } catch (Exception e) {
        }
    }

//    public File getGeneratedFile() {
//        return generatedFile;
//    }
//
//    public void setGeneratedFile(File generatedFile) {
//        this.generatedFile = generatedFile;
//    }

    public boolean isWriteReportToFile()
    {
        return writeReportToFile;
    }

    public void setWriteReportToFile(boolean writeReportToFile)
    {
        this.writeReportToFile = writeReportToFile;
    }

    public String getWriteFolder() {
        return writeFolder;
    }

    public void setWriteFolder(String writeFolder) {
        this.writeFolder = writeFolder;
    }
    
    
}
