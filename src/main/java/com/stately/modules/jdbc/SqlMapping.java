/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.modules.jdbc;

import com.stately.modules.jpa2.QryBuilder;

/**
 *
 * @author Edwin
 */
public class SqlMapping<T>
{
    String fieldName;
    private QryBuilder.FieldType fieldType;
    private T value;

    public SqlMapping(String fieldName, QryBuilder.FieldType fieldType, T value)
    {
        this.fieldName = fieldName;
        this.fieldType = fieldType;
        this.value = value;
    }
        
    public String getFieldName()
    {
        return fieldName;
    }

    public void setFieldName(String fieldName)
    {
        this.fieldName = fieldName;
    }

    public QryBuilder.FieldType getFieldType()
    {
        return fieldType;
    }

    public void setFieldType(QryBuilder.FieldType fieldType)
    {
        this.fieldType = fieldType;
    }

    public T getValue()
    {
        return value;
    }

    public void setValue(T value)
    {
        this.value = value;
    }
    
    
}
