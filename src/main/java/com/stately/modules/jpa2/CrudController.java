package com.stately.modules.jpa2;

import com.google.common.base.Strings;
import com.stately.common.formating.ObjectValue;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;

/**
 *
 * @author Edwin Amoakwa Kwame
 */
public class CrudController implements Serializable
{

    private EntityManager em;

    private Enviroment enviroment = Enviroment.JAVA_EE;

    public String generateId()
    {
        String id = UUID.randomUUID().toString().replaceAll("-", "");
        
        try
        {
            boolean atleastOneAlpha = id.matches(".*[a-zA-Z]+.*");

            if (atleastOneAlpha == false)
            {
                Random rnd = new Random();
                char c1 = (char) (rnd.nextInt(26) + 'a');
                int numToReplace = rnd.nextInt(9);

                id = id.replaceAll(String.valueOf(numToReplace), String.valueOf(c1));

            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        
        
        return id;
    }
    
    
    public void setId(UniqueEntityModel entityModel) {
        if (!Strings.isNullOrEmpty(entityModel.getId())) {
            return;
        }
        entityModel.setId(generateId());
    }
    
    public void setId(UniqueEntityModel2 entityModel) {
        if (!Strings.isNullOrEmpty(entityModel.getId())) {
            return;
        }
        entityModel.setId(generateId());
    }
    
    public void setId(UniqueEntityModel3 entityModel) {
        if (!Strings.isNullOrEmpty(entityModel.getId())) {
            return;
        }
        entityModel.setId(generateId());
    }

    
    public String generateId(Class t)
    {
        String id = generateId();
        
        boolean idExist = idExist(t, id);        
        if(idExist == false)
        {
            return id;
        }
        
        
        //second attempt
        id = generateId();        
        idExist = idExist(t, id);        
        if(idExist == false)
        {
            return id;
        }
        
        id = generateId();
        
        return id;
    }

    public CrudController(EntityManager em)
    {
        this.em = em;
    }

    public EntityManager getEm()
    {
        return em;
    }

    public void setEm(EntityManager em)
    {
        this.em = em;
    }

    private static final Logger LOGGER = Logger.getLogger(CrudController.class.getName());

//    private String lastActivityExceptionMessage = "";
    private String currentUserID;
//    private Exception lastActivityException = null;

    public CrudController()
    {

    }

    public void setCurrentUserID(String currentUserID)
    {
        this.currentUserID = currentUserID;
    }

    @SuppressWarnings("unchecked")
    public <T> T findBySingleField(Class<T> clazz, String field, String value)
    {

        try
        {
            String qry = "SELECT e FROM " + clazz.getSimpleName() + " e "
                    + "WHERE e." + field + " = '" + value + "'";

            List list =  em.createQuery(qry).setMaxResults(1).getResultList();
            
            if(!list.isEmpty())
            {
                return (T) list.get(0);
            }
//            return (T) em.createQuery(qry).getSingleResult();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public <T> T find(Class<T> t, Object id)
    {
        if (id == null)
        {
            return null;
        }

        try
        {

            if (em == null)
            {
                LOGGER.info("Entity Manager is not initialised");
                return null;
            }
            return (T) em.find(t, id);
        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error finding " + t.getName() + " with ID " + id, e);
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public <T> boolean idExist(Class<T> t, Object id)
    {
        if (id == null)
        {
            return false;
        }

        try
        {

            if (em == null)
            {
                LOGGER.info("Entity Manager is not initialised");
                return false;
            }
           QryBuilder builder = new QryBuilder(em, t);
           builder.addReturnField("e."+UniqueEntityModel._id);
           builder.addObjectParam(UniqueEntityModel._id, id);
           
           Object object = builder.getSingleResult(Object.class);
           
           if(object != null)
           {
               return true;
           }
        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error finding " + t.getName() + " with ID " + id, e);
        }

        return false;
    }

    public List findAll(Class t)
    {
        try
        {
            String qry = "SELECT e FROM " + t.getSimpleName() + " e";
            return em.createQuery(qry).getResultList();
        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error finding all " + t, e);
        }

        return Collections.EMPTY_LIST;
    }

    public int count(Class t)
    {
        return count(t, null);
    }

    public int count(Class t, Map<String, Object> values)
    {
        try
        {
            QryBuilder qryBuilder = new QryBuilder(em, t);
            qryBuilder.addReturnField("COUNT(e)");

            if (values != null)
            {
                qryBuilder.map(values);
            }

            return ObjectValue.getIntegerValue(qryBuilder.buildQry().getSingleResult());

        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error finding all " + t, e);
        }

        return 0;
    }

    public <T> T save(Object model)
    {
        try
        {
            return saveEntity(model);
        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error saving model", e);
        }
        return null;
    }

    public List recentlyModified(Class clazz, int maxResult)
    {
        try
        {
            QryBuilder builder = new QryBuilder(em, clazz);
            builder.orderByDesc(UniqueEntityModel._lastModifiedDate);

            return builder.buildQry().setFirstResult(0).setMaxResults(maxResult).getResultList();
        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error saving model", e);
        }
        return Collections.EMPTY_LIST;
    }

    public <T> T hold(Object model)
    {
        try
        {
            if (enviroment != Enviroment.JAVA_SE)
            {
                throw new RuntimeException("Callable only when Enviroment is JAVA_SE");
            }
            
            em.merge(model);
            
            return (T) model;
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    

    public boolean remove(Object model)
    {
        try
        {
            if (enviroment != Enviroment.JAVA_SE)
            {
                throw new RuntimeException("Callable only when Enviroment is JAVA_SE");
            }
            
            em.remove(em.merge(model));
            
            return true;
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return false;
    }
    
    

    public <T> T save(UniqueEntityModel model)
    {

        try
        {
            if (model.getCreatedDate() == null)
            {
                model.setCreatedDate(new Date());
            }
            model.setLastModifiedDate(new Date());
            if (currentUserID != null)
            {
                model.setLastModifiedBy(currentUserID);
            }

//        model.setDeleted("NO");
            model.setUpdated("NO");

            startTransaction();

            if (model.getId() == null)
            {
                model.setId(generateId());                
                em.persist(model);
//            return model;
            } else if (find(model.getClass(), model.getId()) != null)
            {
                em.merge(model);
//            return model;
            } else
            {
                em.persist(model);
//            return model;
            }

            

            commitTransaction();

            return (T) model;
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public <T> T save(UniqueEntityModel2 model)
    {

        try
        {
            if (model.getCreatedDate() == null)
            {
                model.setCreatedDate(new Date());
            }
            model.setLastModifiedDate(new Date());
            if (currentUserID != null)
            {
                model.setLastModifiedBy(currentUserID);
            }

//        model.setDeleted("NO");
            model.setUpdated(false);

            startTransaction();

            if (model.getId() == null)
            {
                model.setId(UUID.randomUUID().toString().replaceAll("-", ""));
                em.persist(model);
//            return model;
            } else if (find(model.getClass(), model.getId()) != null)
            {
                em.merge(model);
//            return model;
            } else
            {
                em.persist(model);
//            return model;
            }
            
            commitTransaction();

            return (T) model;
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public <T> T save(UniqueEntityModel3 model)
    {

        try
        {
            if (model.getCreatedDate() == null)
            {
                model.setCreatedDate(LocalDateTime.now());
            }
            model.setLastModifiedDate(LocalDateTime.now());
            if (currentUserID != null)
            {
                model.setLastModifiedBy(currentUserID);
            }

//        model.setDeleted("NO");
            model.setUpdated(false);

            startTransaction();

            if (model.getId() == null)
            {
                model.setId(UUID.randomUUID().toString().replaceAll("-", ""));
                em.persist(model);
//            return model;
            } else if (find(model.getClass(), model.getId()) != null)
            {
                em.merge(model);
//            return model;
            } else
            {
                em.persist(model);
//            return model;
            }
            
            commitTransaction();

            return (T) model;
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public <T> T save(EntityModel model)
    {
        try
        {
            if (model.getCreatedDate() == null)
            {
                model.setCreatedDate(new Date());
            }
            model.setLastModifiedDate(new Date());
            model.setDeleted("NO");
            model.setUpdated("NO");

            return saveEntity(model);

        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error saving model", e);
        }

        return null;
    }

    public <T> T saveEntity(Object object) 
    {
        if(object == null)
        {
            return null;
        }
        
        startTransaction();

        em.merge(object);
        
        
        commitTransaction();

        return (T) object;
    }

    public boolean update(EntityModel model) 
    {
        if(model == null)
        {
            return false;
        }
        
        try {
            if (model.getCreatedDate() == null) {
                model.setCreatedDate(new Date());
            }
            model.setLastModifiedDate(new Date());
//            model.setLastModifiedBy(currentUserID);
            model.setDeleted("NO");
            model.setUpdated("NO");

            EntityModel entityModel = saveEntity(model);

            if (entityModel != null)
            {
                return true;
            }

        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error saving model", e);
        }

        return false;
    }

    public boolean update(Object model)
    {
        try
        {

            Object entityModel = saveEntity(model);

            if (entityModel != null)
            {
                return true;
            }

        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error saving model", e);
        }

        return false;
    }

    public boolean updateEntity(Object model)
    {
        try
        {

            Object entityModel = saveEntity(model);

            if (entityModel != null)
            {
                return true;
            }

        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error saving model", e);
        }

        return false;
    }

    public boolean update(EntityModel2 model)
    {
        
        if(model == null)
        {
            return false;
        }
        
        try
        {
            if (model.getCreatedDate() == null)
            {
                model.setCreatedDate(new Date());
            }
            model.setLastModifiedDate(new Date());
//            model.setLastModifiedBy(currentUserID);
            model.setDeleted(false);
            model.setUpdated(false);

            Object entityModel = saveEntity(model);

            if (entityModel != null)
            {
                return true;
            }

        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error saving model", e);
        }

        return false;
    }

    public boolean update2(EntityModel2 model) 
    {
        if(model == null)
        {
            return false;
        }
        
        try {
            if (model.getCreatedDate() == null) {
                model.setCreatedDate(new Date());
            }
            model.setLastModifiedDate(new Date());
            model.setDeleted(false);
            model.setUpdated(false);

            Object entityModel = saveEntity(model);

            if (entityModel != null)
            {
                return true;
            }

        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error saving model", e);
        }

        return false;
    }

    public boolean delete(UniqueEntityModel model, boolean permanent) 
    {
        if(model == null)
        {
            return false;
        }
        
        try {
            startTransaction();

            if (permanent == true)
            {
                if (model.getId() != null)
                {
                    em.remove(em.merge(model));

                    LOGGER.log(Level.INFO, " Deleted : {0}", model);
                }

            } else if (permanent == false)
            {
                model.setLastModifiedDate(new Date());

                save(model);

                model.setLastModifiedBy(currentUserID);
                model.setDeleted("YES");
                model.setUpdated("NO");
                em.merge(model);
            }
            
            commitTransaction();
            
            return true;

        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error deleting model", e);
        }

        return false;
    }

    public boolean delete(UniqueEntityModel2 model, boolean permanent) 
    {
        if(model == null)
        {
            return false;
        }
        
        try {
            
            startTransaction();
            
            if (permanent == true)
            {
                if (model.getId() != null)
                {
                    em.remove(em.merge(model));

                    LOGGER.log(Level.INFO, " Deleted : {0}", model);
                }

            } else if (permanent == false)
            {
                model.setLastModifiedDate(new Date());

                save(model);

                model.setLastModifiedBy(currentUserID);
                model.setDeleted(true);
                model.setUpdated(false);
                em.merge(model);
            }
            
            commitTransaction();
            
            return true;

        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error deleting model", e);
        }

        return false;
    }

    public boolean delete(UniqueEntityModel3 model, boolean permanent) 
    {
        if(model == null)
        {
            return false;
        }
        
        try {
            
            startTransaction();
            
            if (permanent == true)
            {
                if (model.getId() != null)
                {
                    em.remove(em.merge(model));

                    LOGGER.log(Level.INFO, " Deleted : {0}", model);
                }

            } else if (permanent == false)
            {
                model.setLastModifiedDate(LocalDateTime.now());

                save(model);

                model.setLastModifiedBy(currentUserID);
                model.setDeleted(true);
                model.setUpdated(false);
                em.merge(model);
            }
            
            commitTransaction();
            
            return true;

        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error deleting model", e);
        }

        return false;
    }

    public boolean delete(Object model) 
    {
        if(model == null)
        {
            return false;
        }
        
        try {
           
            startTransaction();

            em.remove(em.merge(model));
            
            commitTransaction();

            LOGGER.log(Level.INFO, " Deleted : {0}", model);
            
            return true;

        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error deleting model", e);
        }

        return false;
    }

    public boolean deleteEntity(Object model) 
    {
        if(model == null)
        {
            return false;
        }
        
        try {
           
            startTransaction();

            em.remove(model);

            LOGGER.log(Level.INFO, " Deleted : {0}", model);

            commitTransaction();

            return true;

        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error deleting model", e);
        }

        return false;
    }

    public boolean delete(EntityModel model, boolean permanent) 
    {
        if(model == null)
        {
            return false;
        }
        
        startTransaction();
        
        try {
            System.out.println("deleting ... " + model + " with : " + permanent);
            if (permanent == true)
            {
                em.remove(em.merge(model));
                System.out.println("date removed ...");
            } else if (permanent == false)
            {
                model.setLastModifiedDate(new Date());
                model.setLastModifiedBy(currentUserID);
                model.setDeleted("YES");
                model.setUpdated("NO");
                em.merge(model);
            }
            
            commitTransaction();
            
            return true;

        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error saving model", e);
        }

        return false;
    }

    public boolean delete(EntityModel2 model, boolean permanent) 
    {
        if(model == null)
        {
            return false;
        }
        
        startTransaction();
        
        try {
            System.out.println("deleting ... " + model + " with : " + permanent);
            if (permanent == true)
            {
                em.remove(em.merge(model));
                System.out.println("date removed ...");
            } else if (permanent == false)
            {
                model.setLastModifiedDate(new Date());
                model.setLastModifiedBy(currentUserID);
                model.setDeleted(true);
                model.setUpdated(false);
                em.merge(model);
            }
            
            commitTransaction();
            
            return true;

        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE, "Error saving model", e);
        }

        return false;
    }

    
    public boolean startTransaction()
    {
        if (enviroment == Enviroment.JAVA_SE)
        {
            if (em.getTransaction().isActive() == false)
            {
                try
                {
                    em.getTransaction().begin();
                    return true;
                } catch (Exception e)
                {
                    e.printStackTrace();
                    return false;
                }
            }
        }

        return true;
    }
 
    public boolean commitTransaction()
    {
        if (enviroment == Enviroment.JAVA_SE)
        {
            try
            {
                em.flush();
                em.getTransaction().commit();
                return true;
            } catch (Exception e)
            {
                e.printStackTrace();
                return false;
            }
        }

        return true;
    }

    

    public List searchByParameterMap(Map<String, Object> params, Class c)
    {
        String qry = "SELECT e FROM " + c.getSimpleName() + " e WHERE ";

        int itemsCount = params.size();

        int counter = 0;

        for (Map.Entry<String, Object> entry : params.entrySet())
        {
            counter++;

            String string = entry.getKey();
            Object object = entry.getValue();
            
            
            qry += "e." + string + " LIKE '%" + object + "%' ";

            if (counter < itemsCount)
            {
                qry += " AND ";
            }

        }

        System.out.println(qry);

        try
        {
            return em.createQuery(qry).getResultList();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return Collections.EMPTY_LIST;
    }

    public List loadOrderdData(Class c, QryBuilder.OrderBy qo, String orderField, int number)
    {
        try
        {
            String qry = "SELECT e FROM " + c.getSimpleName() + " e "
                    + "ORDER BY e." + orderField + " " + qo.getOrderName();
            return em.createQuery(qry)
                    .setFirstResult(0)
                    .setMaxResults(number)
                    .getResultList();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return Collections.EMPTY_LIST;
    }

    public List loadOrderdData(Class c, QryBuilder.OrderBy qo, String orderField, String selectionField, Object selectionValue, int number)
    {
        try
        {
            String qry = "SELECT e FROM " + c.getSimpleName() + " e "
                    + "WHERE e." + selectionField + " = '" + selectionValue + "' "
                    + "ORDER BY e." + orderField + " " + qo.getOrderName();
            
            return em.createQuery(qry)
                    .setFirstResult(0)
                    .setMaxResults(number)
                    .getResultList();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return Collections.EMPTY_LIST;
    }

    public List loadAllData(String field, String value, String from, Class c)
    {
        try
        {
            String qry = "SELECT e." + from + " FROM " + c.getSimpleName() + " e "
                    + "WHERE e." + field + " = '" + value + "'";
            return em.createQuery(qry).getResultList();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return Collections.EMPTY_LIST;
    }

    public List loadAllDataBy(String field, Class c, String where, String criteriaValue)
    {
        try
        {
            String qry = "SELECT e." + field + " FROM " + c.getSimpleName() + " e "
                    + "WHERE e." + where + " = '" + criteriaValue + "'";
            return em.createQuery(qry).getResultList();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return Collections.EMPTY_LIST;
    }

    public int deleteAll(String field, Object value, Class c)
    {
        return deleteAll(c, field, value);
    }

    public int deleteAll(Class c , String field, Object value)
    {
        try
        {
            startTransaction();
            
            String qry = "DELETE FROM " + c.getSimpleName() + " e "
                    + "WHERE e." + field + " = :value";
            int total = em.createQuery(qry)
                    .setParameter("value", value)
                    .executeUpdate();
            
            commitTransaction();
            
            
            return total;
            
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return 0;
    }
    
    public Integer count(String field, String value, Class c)
    {
        try
        {
            String qry = "SELECT COUNT(e." + field + ") FROM " + c.getSimpleName() + " e "
                    + "WHERE e." + field + " = '" + value + "'";
            Long ct = (Long) em.createQuery(qry).getSingleResult();
            return ct.intValue();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return 0;
    }

    public Enviroment getEnviroment()
    {
        return enviroment;
    }

    public void setEnviroment(Enviroment enviroment)
    {
        this.enviroment = enviroment;
    }

}
