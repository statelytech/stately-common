/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package com.stately.modules.jpa2;

import com.stately.common.api.Selectable;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author Edwin
 */
@MappedSuperclass
public class EntityModel extends Object implements Serializable, Selectable {

    private static final long serialVersionUID = 1L;

    public static final DateFormat SQL_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public static final String _dateCreated = "createdDate";
    public static final String _registrationDate = "registrationDate";
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate = new Date();

    public static final String _lastModifiedDate = "lastModifiedDate";
    @Column(name = "last_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    public static final String _lastModifiedBy = "lastModifiedBy";
    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    @Column(name = "updated")
    private String updated;

    public static final String _deleted = "deleted";
    @Column(name = "deleted")
    private String deleted = "NO";

    @Transient
    private boolean selected = false;

    @Transient
    private int counter = 0;

    public EntityModel() {
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    @Override
    public boolean isSelected() {
        return selected;
    }

    @Override
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @PreUpdate
    @PrePersist
    private void update() {
        setLastModifiedDate(new Date());

        if (createdDate == null) {
            setCreatedDate(new Date());
        }
    }

    public String getModelInfo() {
        return "EntityModel{" + "createdDate=" + createdDate + ", lastModifiedDate=" + lastModifiedDate + ", lastModifiedBy=" + lastModifiedBy + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

    
    public static void countItems(List<? extends EntityModel> itemsList) {
        
        if(itemsList == null)
        {
            return;
        }
        
        int count = 0;
        for (EntityModel item : itemsList) {
            item.setCounter(++count);
        }
    }
}
