/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package com.stately.modules.jpa2;

import com.stately.common.api.Selectable;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author Edwin
 */
@MappedSuperclass
public class EntityModel2 implements Serializable, Selectable {

    private static final long serialVersionUID = 1L;

    public static final DateFormat SQL_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public static final String _created_Date = "createdDate";
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate = new Date();

    public static final String _lastModifiedDate = "lastModifiedDate";
    @Column(name = "last_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;// = new Date();

    public static final String _lastModifiedBy = "lastModifiedBy";
    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    public static final String _updated = "updated";

    @Column(name = "updated")
    private boolean updated;

    public static final String _deleted = "deleted";
    @Column(name = "deleted")
    private boolean deleted;

    @Transient
    private boolean selected = false;

    @Transient    
    private transient int counter = 0;

    public EntityModel2() {
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public boolean getUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    @Override
    public boolean isSelected() {
        return selected;
    }

    @Override
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @PreUpdate
    @PrePersist
    private void update() {
        setLastModifiedDate(new Date());

        if (createdDate == null) {
            setCreatedDate(new Date());
        }
    }

    public String toFullInsertSQL() {
        return null;
    }

    public String toInsertValues() {
        return null;
    }

    public String insertHeader() {
        return null;
    }

    public String updateHeader() {
        return null;
    }

    public String toUpdateValues() {
        return null;
    }

    public String toFullUpdateSQL() {
        return null;
    }

    public String getTableName() {
        return EntityModel2.class.getSimpleName();
    }

    public String updateSql() {
        return null;
    }

    public String deleteQuerry() {
        return null;
    }

    public static void countItems(List<? extends EntityModel2> itemsList) 
    {
        if(itemsList == null)
        {
            return;
        }
        int count = 0;
        for (EntityModel2 item : itemsList) {
            item.setCounter(++count);
        }
    }
}
