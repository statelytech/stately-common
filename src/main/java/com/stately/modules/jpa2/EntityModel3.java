/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package com.stately.modules.jpa2;

import com.stately.common.api.Selectable;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;

/**
 *
 * @author Edwin
 */
@MappedSuperclass
public class EntityModel3 implements Serializable, Selectable {

    private static final long serialVersionUID = 1L;

    public static final DateFormat SQL_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public static final String _createdDate = "createdDate";
    @Column(name = "created_date")
    private LocalDateTime createdDate = LocalDateTime.now();

    public static final String _lastModifiedDate = "lastModifiedDate";
    @Column(name = "last_modified_date")
    private LocalDateTime lastModifiedDate;// = LocalDateTime.now();

    public static final String _lastModifiedBy = "lastModifiedBy";
    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    public static final String _updated = "updated";

    @Column(name = "updated")
    private boolean updated;

    public static final String _deleted = "deleted";
    @Column(name = "deleted")
    private boolean deleted;

    @Transient
    private boolean selected = false;

    @Transient
    private int counter = 0;

    public EntityModel3() {
    }

    public boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public boolean getUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    @Override
    public boolean isSelected() {
        return selected;
    }

    @Override
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @PreUpdate
    @PrePersist
    private void update() {
        setLastModifiedDate(LocalDateTime.now());

        if (createdDate == null) {
            setCreatedDate(LocalDateTime.now());
        }
    }

    public String getModelInfo() {
        return "EntityModel{" + "createdDate=" + createdDate + ", lastModifiedDate=" + lastModifiedDate + ", lastModifiedBy=" + lastModifiedBy + ", updated=" + updated + ", deleted=" + deleted + '}';
    }

    public String toFullInsertSQL() {
        return null;
    }

    public String toInsertValues() {
        return null;
    }

    public String insertHeader() {
        return null;
    }

    public String updateHeader() {
        return null;
    }

    public String toUpdateValues() {
        return null;
    }

    public String toFullUpdateSQL() {
        return null;
    }

    public String getTableName() {
        return EntityModel3.class.getSimpleName();
    }

    public String updateSql() {
        return null;
    }

    public String deleteQuerry() {
        return null;
    }

    public static void countItems(List<? extends EntityModel3> itemsList) 
    {
        if(itemsList == null)
        {
            return;
        }
        
        int count = 0;
        for (EntityModel3 item : itemsList) {
            item.setCounter(++count);
        }
    }
}
