/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stately.modules.jpa2;

import com.stately.common.formating.ObjectValue;
import com.stately.common.model.DateRange;
import com.stately.common.model.LocalDateRange;
import com.stately.common.model.LocalDateTimeRange;
import com.stately.common.model.NumberRange;
import com.stately.common.utils.StringUtil;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author Edwin
 * @email edwin.amoakwa@gmail.com
 */
public class QryBuilder implements Serializable
{

    private static final Logger LOGGER = Logger.getLogger(QryBuilder.class.getName());
    private EntityManager em;
    private boolean showLog = false;

    private String qryString;
    private String className;

    private String currentQryGroup = null;
    

    private List<QryParam> qryParams = new LinkedList<>();
    private List<QryOrder> ordering = new LinkedList<>();
    private List<String> returnFieldsList = new LinkedList<>();
    private List<String> groupFields = new LinkedList<>();

    private String var = "e";
    private Class rootClass;
    
    public QryBuilder()
    {
    }

    public QryBuilder(EntityManager em)
    {
        this.em = em;
    }

    public QryBuilder(EntityManager em, Class rootClass)
    {
        this.em = em;
        setQryClass(rootClass);
    }

    public QryBuilder(EntityManager em, Class rootClass, String returnVariable)
    {
        this.em = em;
        setQryClass(rootClass);
        returnFieldsList.add(var + "." + returnVariable);

    }

    public static QryBuilder get(EntityManager em)
    {
        return new QryBuilder(em);
    }

    public String getCurrentQryGroup()
    {
        return currentQryGroup;
    }

    public void setCurrentQryGroup(String currentQryGroup)
    {
        this.currentQryGroup = currentQryGroup;
    }

    public void setQryClass(Class qryClass)
    {
        rootClass = qryClass;
        className = qryClass.getSimpleName();
    }

    public QryBuilder add(QryParam param)
    {
        if (currentQryGroup != null)
        {
            if (showLog)
            {
                LOGGER.log(Level.INFO, "Current QryGroup is not null, adding {0}", currentQryGroup);
            }
            param.setQryGroup(currentQryGroup);
        }
        if (showLog)
        {
            System.out.println("QRY_GROUP = " + currentQryGroup);
        }
        qryParams.add(param);
        return this;
    }

    public void map(Map<String, Object> values)
    {
        map(values, false);
    }

    public void map(Map<String, Object> values, boolean order)
    {
        if (values == null)
        {
            return;
        }

        for (Map.Entry<String, Object> entry : values.entrySet())
        {
            String field = entry.getKey();
            Object value = entry.getValue();
            
            if(value == null)
            {
                continue;
            }
            
            if (value instanceof String)
            {
                addStringQryParam(field, value, QryBuilder.ComparismCriteria.EQUAL);
            } else if (value instanceof Date)
            {
                addDateParam(field, (Date) value, ComparismCriteria.EQUAL);
            } else if (value instanceof LocalDate)
            {
                //TODO localdate instance
                addLocalDateParam(field, (LocalDate) value, ComparismCriteria.LIKE);
            } else
            {
                addObjectParam(field, value);
            }

            if (order)
            {
                orderByAsc(field);
            }

        }
    }

    public QryBuilder addStringQryParam(String field, Object value, ComparismCriteria includeCriteria)
    {
        QryParam qryParam = new QryParam(field, value, FieldType.String, includeCriteria);
        add(qryParam);
        return this;
    }

    public QryBuilder addStringQryParam(String field, Object value, ComparismCriteria comparismCriteria, IncludeCriteria includeCriteria)
    {
        QryParam qryParam = new QryParam(field, value, FieldType.String, comparismCriteria, includeCriteria);
        if (showLog)
        {
            System.out.println("going to add = " + qryParam);
        }
        add(qryParam);
        return this;
    }

    public QryBuilder addDateParam(String field, Date value, ComparismCriteria includeCriteria)
    {
        QryParam qryParam = new QryParam(field, value, FieldType.Date, includeCriteria);
        add(qryParam);
        return this;
    }

    public QryBuilder addDateParam(String field, LocalDate value, ComparismCriteria includeCriteria)
    {
        QryParam qryParam = new QryParam(field, value, FieldType.LocalDate, includeCriteria);
        add(qryParam);
        return this;
    }
    
    public QryBuilder addDateBetweenInclusive(String startDateField, LocalDate valueDate, String endDateField)
    {
        addDateParam(startDateField, valueDate,QryBuilder.ComparismCriteria.LESS_THAN_OR_EQUAL);

        addDateParam(endDateField,valueDate,QryBuilder.ComparismCriteria.GREATER_THAN_OR_EQUAL);
       
        return this;
    }


    public QryBuilder addDateTimeParam(String field, Date value, ComparismCriteria includeCriteria)
    {
        QryParam qryParam = new QryParam(field, value, FieldType.DateTime, includeCriteria);
        add(qryParam);
        return this;
    }

    public QryBuilder addDateTimeParam(String field, LocalDateTime value, ComparismCriteria includeCriteria)
    {
        QryParam qryParam = new QryParam(field, value, FieldType.LocalDateTime, includeCriteria);
        add(qryParam);
        return this;
    }

    public QryBuilder addLocalDateParam(String field, LocalDate value, ComparismCriteria includeCriteria)
    {
        QryParam qryParam = new QryParam(field, value, FieldType.LocalDate, includeCriteria);
        add(qryParam);
        return this;
    }

    public QryBuilder addNumberParam(String field, Number value, ComparismCriteria includeCriteria)
    {
        QryParam qryParam = new QryParam(field, value, FieldType.Number, includeCriteria);
        add(qryParam);
        return this;
    }

    public QryBuilder addDateRange(DateRange dateRange, String fieldName)
    {
        if (dateRange != null)
        {
            if (dateRange.getFromDate() != null)
            {
                addDateParam(fieldName, dateRange.getFromDate(), QryBuilder.ComparismCriteria.GREATER_THAN_OR_EQUAL);
            }
            if (dateRange.getToDate() != null)
            {
                addDateParam(fieldName, dateRange.getToDate(), QryBuilder.ComparismCriteria.LESS_THAN_OR_EQUAL);
            }
        }
        return this;
    }

    public QryBuilder addDateRange(LocalDateRange dateRange, String fieldName)
    {
        if (dateRange != null)
        {
            if (dateRange.getFromDate() != null)
            {
                addLocalDateParam(fieldName, dateRange.getFromDate(), QryBuilder.ComparismCriteria.GREATER_THAN_OR_EQUAL);
            }
            if (dateRange.getToDate() != null)
            {
                addLocalDateParam(fieldName, dateRange.getToDate(), QryBuilder.ComparismCriteria.LESS_THAN_OR_EQUAL);
            }
        }
        return this;
    }

    public QryBuilder addDateTimeRange(DateRange dateRange, String fieldName)
    {
        if (dateRange != null)
        {
            if (dateRange.getFromDate() != null)
            {
                addDateTimeParam(fieldName, dateRange.getFromDate(), QryBuilder.ComparismCriteria.GREATER_THAN_OR_EQUAL);
            }
            if (dateRange.getToDate() != null)
            {
                addDateTimeParam(fieldName, dateRange.getToDate(), QryBuilder.ComparismCriteria.LESS_THAN_OR_EQUAL);
            }
        }
        return this;
    }
    public QryBuilder addDateTimeRange(LocalDateTimeRange dateRange, String fieldName)
    {
        if (dateRange != null)
        {
            if (dateRange.getFromDate() != null)
            {
                addDateTimeParam(fieldName, dateRange.getFromDate(), QryBuilder.ComparismCriteria.GREATER_THAN_OR_EQUAL);
            }
            if (dateRange.getToDate() != null)
            {
                addDateTimeParam(fieldName, dateRange.getToDate(), QryBuilder.ComparismCriteria.LESS_THAN_OR_EQUAL);
            }
        }
        return this;
    }

    public QryBuilder addRange(Number start, Number end, String field)
    {

        if (start != null)
        {
            addNumberParam(field, start, ComparismCriteria.GREATER_THAN_OR_EQUAL);
        }

        if (end != null)
        {
            addNumberParam(field, end, ComparismCriteria.LESS_THAN_OR_EQUAL);
        }

        return this;
    }

    public QryBuilder addNumberRange(NumberRange numberRange, String field)
    {
        if (numberRange == null)
        {
            return this;
        }

        if (numberRange.getMinimum() != null)
        {
            addNumberParam(field, numberRange.getMinimum(), ComparismCriteria.GREATER_THAN_OR_EQUAL);
        }

        if (numberRange.getMaximum() != null)
        {
            addNumberParam(field, numberRange.getMaximum(), ComparismCriteria.LESS_THAN_OR_EQUAL);
        }

        return this;
    }

    public QryBuilder addObjectParam(String field, Object value)
    {
        QryParam qryParam = new QryParam(field, value, FieldType.OBJECT, ComparismCriteria.EQUAL);
        add(qryParam);
        return this;
    }

    public QryBuilder addObjectParamWhenNotNull(String field, Object value)
    {
        if(value == null)
        {
            return this;
        }
        
        QryParam qryParam = new QryParam(field, value, FieldType.OBJECT, ComparismCriteria.EQUAL);
        add(qryParam);
        return this;
    }

    public QryBuilder addRawParam(String qryString)
    {
        QryBuilder.QryParam qryParam = new QryBuilder.QryParam();
        qryParam.setQryString(qryString);
        qryParam.setRawParam(true);
        
        add(qryParam);
        return this;
    }

    public QryBuilder withRawObjectParam(String field, Object value)
    {
        return withRawParam(field,value, FieldType.OBJECT);
    }

    public QryBuilder withRawParam(String field, Object value, FieldType fieldType)
    {
//        QryParam qryParam = new QryParam(field, value, fieldType);
        QryParam qryParam = new QryParam();
//        qryParam.field = field;
//        qryParam.value = 
//        qryParam.comparism = comparismCriteria;
        qryParam.fieldType = fieldType;
        qryParam.setRawParam(true);
        qryParam.paramVar = field;
        qryParam.value = value;
        add(qryParam);
        return this;
    }

    public QryBuilder addInParam(String field, List value)
    {
        if(value == null || value.isEmpty())
        {
            return this;
        }
        QryParam qryParam = new QryParam(field, value, FieldType.OBJECT, ComparismCriteria.IN);
        add(qryParam);
        return this;
    }

    public QryBuilder addNotInParam(String field, List value)
    {
        if(value == null || value.isEmpty())
        {
            return this;
        }
        QryParam qryParam = new QryParam(field, value, FieldType.OBJECT, ComparismCriteria.NOT_IN);
        add(qryParam);
        return this;
    }

    public QryBuilder addObjectParamNotEqual(String field, Object value)
    {
        QryParam qryParam = new QryParam(field, value, FieldType.OBJECT, ComparismCriteria.NOT);
        add(qryParam);
        return this;
    }

    public QryBuilder addReturnField(String field)
    {
        returnFieldsList.add(field);
        return this;
    }

    /*
     * the field eg. valueDate
     */
    public QryBuilder addGroupBy(String field)
    {
        groupFields.add(field);
        return this;
    }

    public QryBuilder orderBy(QryOrder qryOrder)
    {
        ordering.add(qryOrder);
        return this;
    }

    public QryBuilder orderByAsc(String field)
    {
        ordering.add(new QryOrder(field, OrderBy.ASC));
        return this;
    }

    public QryBuilder orderByDesc(String field)
    {
        ordering.add(new QryOrder(field, OrderBy.DESC));
        return this;
    }

    public QryBuilder printQryInfo()
    {
        System.out.println(getQryInfo());
        return this;
    }
    
    public String getQryInfo()
    {
        StringBuilder builder = new StringBuilder();
        builder.append(getQryString()).append("\n");
        builder.append(getParamString());

        return builder.toString();
    }

    public String getParamString()
    {
        StringBuilder builder = new StringBuilder();

        for (QryParam qryParam : qryParams)
        {
            if(qryParam.isRawParam())
            {
                continue;
            }
            
            builder.append(qryParam.getField()).append(" : ").append(qryParam.getValue()).append("\n");
        }

        return builder.toString();
    }

    public String getFinalString()
    {
        return qryString;
    }

    public String getQryString()
    {
        //forming select group

        if (returnFieldsList.isEmpty())
        {
            qryString = "SELECT e FROM " + className + " e ";
        } else
        {
            String obj = "";
            int size = returnFieldsList.size();
            int counter = 0;
            for (String object : returnFieldsList)
            {
                counter++;
                obj += object;

                if (counter != size)
                {
                    obj += ",";
                }
            }
            qryString = "SELECT " + obj + " FROM " + className + " e ";
        }

        buildParameterString();

        return qryString;

    }

    public String buildParameterString()
    {

        if (qryParams.size() > 0)
        {
            qryString = qryString + " WHERE ";
        }

        if (showLog)
        {
            System.out.println("LIST IS " + qryParams);
            System.out.println("param size = " + qryParams.size());
        }
        
        int counter = 0;
        
        for (QryParam qryParam : qryParams)
        {
            //add to cater for the introduction of withRaw**Params
            if(StringUtil.isNullOrEmpty(qryParam.createQry()))
            {
                continue;
            }
            qryString +=qryParam.createQry();
            
            qryParam.used = true;
            counter++;

            if (counter < qryParams.size())
            {
                qryString += " AND ";
            }
        }

        //forming group
        int groupCounter = 0;
        int groupSize = groupFields.size();
        if (groupFields.isEmpty() == false)
        {
            qryString += " GROUP BY ";

            for (String groupField : groupFields)
            {
                qryString += " e." + groupField + " ";

                groupCounter++;

                if (groupCounter != groupSize)
                {
                    qryString += " , ";
                }
            }
        }

        //forming order by
        int orderCounter = 0;
        int orderSize = ordering.size();
        if (ordering.isEmpty() == false)
        {
            qryString += " ORDER BY ";

            for (QryOrder qryOrder : ordering)
            {

                if (qryOrder.orderField.contains("(") && qryOrder.orderField.contains(")"))
                {
                    qryOrder.addVariable = false;
                }

                if (qryOrder.addVariable)
                {
                    qryString += " " + var + "." + qryOrder.orderField + " " + qryOrder.orderBy.getOrderName();
                } else
                {
                    qryString += " " + qryOrder.orderField + " " + qryOrder.orderBy.getOrderName();
                }

                orderCounter++;

                if (orderCounter != orderSize)
                {
                    qryString += " , ";
                }
            }
        }

        return qryString;
    }

    public String createParameterString()
    {
        String qryParamString = "";
//        System.out.println("param size ... " + qryParams.size());
        if (qryParams.isEmpty())
        {
            return qryParamString;
        }

        qryParamString = qryParamString + " WHERE ";
        if (showLog)
        {
            System.out.println("LIST IS " + qryParams);
            System.out.println("param size = " + qryParams.size());
        }

        int counter = 0;

        for (QryParam qryParam : qryParams)
        {
            qryParamString += qryParam.createQry();
//            if (qryParam.getValue() == null && qryParam.comparism == ComparismCriteria.NOT)
//            {
//                qryParamString += "e." + qryParam.field + " IS NOT NULL ";
//            } else if (qryParam.getValue() == null)
//            {
//                qryParamString += "e." + qryParam.field + " IS NULL ";
//            } else
//            {
//                
//                if (qryParam.fieldType == FieldType.String)
//                {
//                    if (qryParam.comparism == ComparismCriteria.LIKE)
//                    {
//                        qryParamString += "e." + qryParam.field + " LIKE '%" + qryParam.value + "%' ";
//                    }
//
//                    if (qryParam.comparism == ComparismCriteria.EQUAL)
//                    {
//                        qryParamString += "e." + qryParam.field + " = '" + qryParam.value + "' ";
//                    }
//                }
//                if (qryParam.fieldType == FieldType.Number)
//                {
//                    if (qryParam.comparism != ComparismCriteria.LIKE)
//                    {
//                        qryParamString += "e." + qryParam.field + " " + qryParam.comparism.getSign() + " " + qryParam.value + " ";
//                    }
//                }
//
//                if (qryParam.fieldType == FieldType.Date || qryParam.fieldType == FieldType.DateTime)
//                {
//                    qryParamString += "e." + qryParam.field + " " + qryParam.comparism.getSign() + ":" + qryParam.paramVar + " ";
//                }
//
//                if (qryParam.fieldType == FieldType.LocalDate)
//                {
//                    qryParamString += "e." + qryParam.field + " " + qryParam.comparism.getSign() + ":" + qryParam.paramVar + " ";
//                }
//                
//                if (qryParam.fieldType == FieldType.LocalDateTime)
//                {
//                    qryParamString += "e." + qryParam.field + " " + qryParam.comparism.getSign() + ":" + qryParam.paramVar + " ";
//                }
//
//                if (qryParam.fieldType == FieldType.OBJECT)
//                {
//                    qryParamString += "e." + qryParam.field + " " + qryParam.comparism.getSign() + ":" + qryParam.paramVar + " ";
//                }
//            }

            qryParam.used = true;
            counter++;

            if (counter < qryParams.size())
            {
                qryParamString += " AND ";
            }
        }

        return qryParamString;
    }

    public Query applyParameters(Query query)
    {

        for (QryParam qryParam : qryParams)
        {
            query = applyParameters(query, qryParam);
        }

        return query;
    }

    public Query applyParameters(Query query, QryParam qryParam)
    {
        return QryBuilder.applyQueryParameters(query, qryParam);
    }

    public static Query applyQueryParameters(Query query, QryParam qryParam)
    {
        if(qryParam.rawParam == true)
        {
            return query;
        }
        
        if (qryParam.getValue() == null)
        {
            return query;
        } else if (qryParam.getFieldType() == FieldType.Date)
        {
            query.setParameter(qryParam.paramVar, (Date) qryParam.getValue(), TemporalType.DATE);
        } else if (qryParam.getFieldType() == FieldType.LocalDate)
        {
            query.setParameter(qryParam.paramVar, (LocalDate) qryParam.getValue());
        } else if (qryParam.getFieldType() == FieldType.LocalDateTime)
        {
            query.setParameter(qryParam.paramVar, (LocalDateTime) qryParam.getValue());
        } else if (qryParam.getFieldType() == FieldType.DateTime)
        {
            query.setParameter(qryParam.paramVar, (Date) qryParam.getValue(), TemporalType.TIMESTAMP);
        } else if (qryParam.getFieldType() == FieldType.OBJECT)
        {
            query.setParameter(qryParam.paramVar, qryParam.getValue());
        }

        return query;
    }

    public int count(Map<String, Object> map)
    {
        try
        {
            for (String key : map.keySet())
            {
                addObjectParam(key, map.get(key));
            }

            qryString = "SELECT COUNT(e) FROM " + className + " e ";

            buildParameterString();

            Query query = em.createQuery(qryString);
            applyParameters(query);

            if (showLog)
            {
                System.out.println(qryString);
                System.out.println(getParamString());

            }

            return ObjectValue.get_intValue(query.getSingleResult());
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        return 0;
    }

    public int count()
    {
        return count(new HashMap<String, Object>());
    }

    public int delete(Map<String, Object> map)
    {

        try
        {
            for (String key : map.keySet())
            {
                addObjectParam(key, map.get(key));
            }

            qryString = "DELETE FROM " + className + " " + var + " ";

            buildParameterString();

            Query query = em.createQuery(qryString);
            applyParameters(query);

            if (showLog)
            {
                System.out.println(qryString);
                System.out.println(getParamString());

            }

            return ObjectValue.get_intValue(query.executeUpdate());
        } catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        return 0;
    }

    public int delete()
    {
        return delete(new HashMap<String, Object>());
    }

    public Query buildQry()
    {

        qryString = getQryString();

        if (showLog)
        {
            System.out.println(qryString);
        }

        if (em == null)
        {
            LOGGER.log(Level.SEVERE, "Entity Manager could not be initialised");
        }

        Query query = em.createQuery(qryString);

        applyParameters(query);

        return query;
    }
    
    public List processQry()
    {
        List list =  buildQry().getResultList();
                
        return new LinkedList(list);
    }

    public QryBuilder showLog(boolean showLog)
    {
        this.showLog = showLog;

        return this;
    }

    public enum ComparismCriteria
    {
        LIKE(" % "),
        EQUAL(" = "),
        LESS_THAN(" < "),
        GREATER_THAN(" > "),
        GREATER_THAN_OR_EQUAL(" >= "),
        LESS_THAN_OR_EQUAL(" <= "),
        NOT(" != "),
        IN(" IN "),
        NOT_IN(" NOT IN ");

        ComparismCriteria(String sign)
        {
            this.sign = sign;
        }

        private String sign;

        private String getSign()
        {
            return sign;
        }
    }

    public enum IncludeCriteria
    {

        AND("AND"),
        OR("OR");

        IncludeCriteria(String sign)
        {
            this.sign = sign;
        }

        private String sign;

        public String getSign()
        {
            return sign;
        }

    }

    public enum FieldType
    {
        String, Number, Date, OBJECT, LocalDate, LocalDateTime, DateTime
    }

    public static class QryOrder implements Serializable
    {

        private String orderField;
        private boolean addVariable = true;
        private OrderBy orderBy;

        public QryOrder()
        {
        }

        public QryOrder(String orderField, OrderBy orderBy)
        {
            this.orderField = orderField;
            this.orderBy = orderBy;
        }

        public QryOrder(String orderField, OrderBy orderBy, boolean addVariable)
        {
            this.orderField = orderField;
            this.orderBy = orderBy;
            this.addVariable = addVariable;
        }

    }

    public static class QryParam implements Serializable
    {
        private Map<String, Object> parameters = null;

        public static final String DEFAULT_QRY_GROUP = "default";
        private boolean used;
        private boolean rawParam;
        private String field;
        private String qryString = "";
        private Object value;
        private FieldType fieldType;
        private ComparismCriteria comparism;
        private String qryGroup = DEFAULT_QRY_GROUP;
        private IncludeCriteria includeCriteria = IncludeCriteria.AND;

        private String paramVar;
        private QryParam qryParam;

        public QryParam(String field, Object value, FieldType fieldType, ComparismCriteria includeCriteria)
        {
            this.field = field;
            this.value = value;
            this.fieldType = fieldType;
            this.comparism = includeCriteria;

            paramVar = field.replaceAll("\\.", "") + Math.abs(Objects.hashCode(value));
            qryParam = this;

            build();

        }

        public QryParam(String field, Object value, FieldType fieldType, ComparismCriteria comparismCriteria, IncludeCriteria includeCriteria1)
        {
            this.field = field;
            this.value = value;
            this.fieldType = fieldType;
            this.comparism = comparismCriteria;
            this.includeCriteria = includeCriteria1;

            paramVar = field.replaceAll("\\.", "") + Math.abs(Objects.hashCode(value));

            qryParam = this;

            build();
        }
        
        public QryParam()
        {
            
        }

        private QryParam(String name, Object value)
        {
            this.parameters = new HashMap<>();
            this.parameters.put(name, value);

            qryParam = this;

            build();
        }

        public static QryParam with(String name, Object value)
        {
            return new QryParam(name, value);
        }

        public QryParam and(String name, Object value)
        {
            this.parameters.put(name, value);
            return this;
        }

        public Map<String, Object> parameters()
        {
            return this.parameters;
        }

        public boolean isUsed()
        {
            return used;
        }

        public String getField()
        {
            return field;
        }

        public Object getValue()
        {
            return value;
        }

        public FieldType getFieldType()
        {
            return fieldType;
        }

        public ComparismCriteria getComparism()
        {
            return comparism;
        }

        public String getQryGroup()
        {
            return qryGroup;
        }

        public void setQryGroup(String qryGroup)
        {
            this.qryGroup = qryGroup;
        }

        public String getQryString()
        {
            return qryString;
        }

        public void setQryString(String qryString)
        {
            this.qryString = qryString;
        }
        
        public boolean isRawParam()
        {
            return rawParam;
        }

        public void setRawParam(boolean rawParam)
        {
            this.rawParam = rawParam;
        }

        @Override
        public String toString()
        {
            return "QryParam{ field=" + field + ", value=" + value + ", comparism=" + comparism + '}';
        }

        public String createQry()
        {
            if(rawParam)
            {
                return qryString;
            }
            
            build();
            return qryString;
        }
        
        public void build()
        {
            if (rawParam)
            {
                return;
            }
             
            qryString = "";
            
            if (qryParam.getValue() == null && qryParam.comparism == ComparismCriteria.NOT)
            {
                qryString += "e." + qryParam.field + " IS NOT NULL ";
            } else if (qryParam.getValue() == null)
            {
                qryString += "e." + qryParam.field + " IS NULL ";
            } else
            {
                if (qryParam.fieldType == FieldType.String)
                {
                    if (qryParam.comparism == ComparismCriteria.LIKE)
                    {
                        qryString += "e." + qryParam.field + " LIKE '%" + qryParam.value + "%' ";
                    }

                    if (qryParam.comparism == ComparismCriteria.EQUAL)
                    {
                        qryString += "e." + qryParam.field + " = '" + qryParam.value + "' ";
                    }
                }
                if (qryParam.fieldType == FieldType.Number)
                {
                    if (qryParam.comparism != ComparismCriteria.LIKE)
                    {
                        qryString += "e." + qryParam.field + " " + qryParam.comparism.getSign() + " " + qryParam.value + " ";
                    }
                }

                if (qryParam.fieldType == FieldType.Date || qryParam.fieldType == FieldType.DateTime)
                {
                    qryString += "e." + qryParam.field + " " + qryParam.comparism.getSign() + ":" + qryParam.paramVar + " ";
                }

                if (qryParam.fieldType == FieldType.LocalDate)
                {
                    qryString += "e." + qryParam.field + " " + qryParam.comparism.getSign() + ":" + qryParam.paramVar + " ";
                }

                if (qryParam.fieldType == FieldType.LocalDateTime)
                {
                    qryString += "e." + qryParam.field + " " + qryParam.comparism.getSign() + ":" + qryParam.paramVar + " ";
                }

                if (qryParam.fieldType == FieldType.OBJECT)
                {
                    qryString += "e." + qryParam.field + " " + qryParam.comparism.getSign() + ":" + qryParam.paramVar + " ";
                }
            }
        }

    }

    

    public static enum OrderBy implements Serializable
    {

        ASC("ASC"), DESC("DESC");
        private String orderName;

        private OrderBy(String orderName)
        {
            this.orderName = orderName;
        }

        public String getOrderName()
        {
            return orderName;
        }
    }

    public static class QrySearchOrder implements Serializable
    {

        private String searchField;
        private OrderBy orderBy;

        public java.lang.String getSearchField()
        {
            return searchField;
        }

        public void setSearchField(java.lang.String searchField)
        {
            this.searchField = searchField;
        }

        public OrderBy getOrderBy()
        {
            return orderBy;
        }

        public void setOrderBy(OrderBy orderBy)
        {
            this.orderBy = orderBy;
        }

        public static List<QryBuilder.QrySearchOrder> createOrderList(int number)
        {
            List<QryBuilder.QrySearchOrder> qrySearchOrdersList = new LinkedList<>();
            for (int i = 0; i < number; i++)
            {
                qrySearchOrdersList.add(new QrySearchOrder());
            }

            return qrySearchOrdersList;
        }

    }

    @SuppressWarnings("unchecked")
    public <T> T getSingleResult(Class<T> t)
    {
        try
        {
            List<T> list = buildQry()
                    .setFirstResult(0)
                    .setMaxResults(1)
                    .getResultList();

            if (list != null && !list.isEmpty())
            {
                return list.get(0);
            }

        } catch (Exception e)
        {
            String msg = "Error finding loading single result of class " + t.getName()
                    + " em: " + em + " - " + e.toString()
                    + " --\n " + getQryInfo();
            LOGGER.log(Level.SEVERE, msg);
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public double getResultAsDouble()
    {
        try
        {
            Object value = getSingleResult(Object.class);
            
            return ObjectValue.get_doubleValue(value);
            
        } catch (Exception e)
        {
            String msg = "Error finding loading single result of class " 
                    + " em: " + em + " - " + e.toString()
                    + " --\n " + getQryInfo();
            LOGGER.log(Level.SEVERE, msg);
        }

        return 0.0;
    }

    @SuppressWarnings("unchecked")
    public long getResultAsLong()
    {
        try
        {
            Object value = getSingleResult(Object.class);
            
            return ObjectValue.get_longValue(value);
            
        } catch (Exception e)
        {
            String msg = "Error finding loading single result of class " 
                    + " em: " + em + " - " + e.toString()
                    + " --\n " + getQryInfo();
            LOGGER.log(Level.SEVERE, msg);
        }

        return 0;
    }

    @SuppressWarnings("unchecked")
    public int getResultAsInt()
    {
        try
        {
            Object value = getSingleResult(Object.class);
            
            return ObjectValue.get_intValue(value);
            
        } catch (Exception e)
        {
            String msg = "Error finding loading single result of class " 
                    + " em: " + em + " - " + e.toString()
                    + " --\n " + getQryInfo();
            LOGGER.log(Level.SEVERE, msg);
        }

        return 0;
    }

    
    public static QryBuilder get(EntityManager em, Class rootClass)
    {
        QryBuilder qryBuilder = new QryBuilder(em, rootClass);

        return qryBuilder;
    }

    public static void main(String[] args)
    {
        QryBuilder qryBuilder = new QryBuilder(null, QryBuilder.class);
        qryBuilder.showLog = true;
        qryBuilder.addObjectParam("dddd", "ddd");
        qryBuilder.addObjectParam("aaa", "aaa");

        System.out.println(" ppppppp ");
        System.out.println(qryBuilder.createParameterString());
//        qryBuilder.applyParameters(query);

        String param = "dd(dsd)";
        String regrex = "\\([a-zA-Z ]+\\)";

        boolean matches = param.matches(regrex);

        System.out.println(matches);
    }

    /**
     *
     */
    public List findAllOrderAsc(Class t, String orderField)
    {
        try
        {
            String qry = "SELECT e FROM " + t.getSimpleName() + " e "
                    + "ORDER BY e." + orderField;
            return em.createQuery(qry).getResultList();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return Collections.EMPTY_LIST;
    }

    public List loadDataByField(String searchField, Object fieldValue, Class c)
    {
        try
        {
            String qry = "SELECT e FROM " + c.getSimpleName() + " e "
                    + "WHERE e." + searchField + " =:fieldValue ";
            return em.createQuery(qry)
                    .setParameter("fieldValue", fieldValue).getResultList();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return Collections.EMPTY_LIST;
    }

    public List loadOrderdDataByField(String searchField, Object fieldValue, Class c, String orderField)
    {
        try
        {
            String qry = "SELECT e FROM " + c.getSimpleName() + " e "
                    + "WHERE e." + searchField + " =:fieldValue "
                    + "ORDER BY e." + orderField;
            return em.createQuery(qry)
                    .setParameter("fieldValue", fieldValue).getResultList();

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return Collections.EMPTY_LIST;
    }

    public <T> T find(Class<T> t, String field, Object fieldValue)
    {
        try
        {
            String qry = "SELECT e FROM " + t.getSimpleName() + " e "
                    + "WHERE e." + field + " =:fieldValue ";
            return (T) em.createQuery(qry)
                    .setParameter("fieldValue", fieldValue).getSingleResult();
        } catch (Exception e)
        {
//            LOGGER.log(Level.SEVERE, "Unable to find " + t + " with "+field+" = " + fieldValue, e);
        }

        return null;
    }

    
}
