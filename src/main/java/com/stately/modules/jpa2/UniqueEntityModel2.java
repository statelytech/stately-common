package com.stately.modules.jpa2;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 *
 * @author Edwin
 */

@MappedSuperclass
public class UniqueEntityModel2 extends EntityModel2 implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    public static final String _id = "id";
    @Id
    @Basic(optional = false)
    @Column(name = "id",length = 50)
    private String id;
    
    public UniqueEntityModel2()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final UniqueEntityModel2 other = (UniqueEntityModel2) obj;
        if (!Objects.equals(this.id, other.id))
        {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }
    

}
